<h1>Awesome Go</h1>

<p><a href="https://travis-ci.org/avelino/awesome-go"><img src="https://travis-ci.org/avelino/awesome-go.svg?branch=master" alt="Build Status" /></a> <a href="https://github.com/sindresorhus/awesome"><img src="https://cdn.rawgit.com/sindresorhus/awesome/d7305f38d29fed78fa85652e3a63e154dd8e8829/media/badge.svg" alt="Awesome" /></a> <a href="http://gophers.slack.com/messages/awesome"><img src="https://img.shields.io/badge/join-us%20on%20slack-gray.svg?longCache=true&amp;logo=slack&amp;colorB=red" alt="Slack Widget" /></a></p>

<p>A curated list of awesome Go frameworks, libraries and software. Inspired by <a href="https://github.com/vinta/awesome-python">awesome-python</a>.</p>

<h3>Contributing</h3>

<p>Please take a quick gander at the <a href="https://github.com/avelino/awesome-go/blob/master/CONTRIBUTING.md">contribution guidelines</a> first. Thanks to all <a href="https://github.com/avelino/awesome-go/graphs/contributors">contributors</a>; you rock!</p>

<h4><em>If you see a package or project here that is no longer maintained or is not a good fit, please submit a pull request to improve this file. Thank you!</em></h4>

<h3>Contents</h3>

<ul>
<li><p><a href="#awesome-go">Awesome Go</a></p>

<ul>
<li><a href="#audio-and-music">Audio and Music</a></li>
<li><a href="#authentication-and-oauth">Authentication and OAuth</a></li>
<li><a href="#bot-building">Bot Building</a></li>
<li><a href="#command-line">Command Line</a></li>
<li><a href="#configuration">Configuration</a></li>
<li><a href="#continuous-integration">Continuous Integration</a></li>
<li><a href="#css-preprocessors">CSS Preprocessors</a></li>
<li><a href="#data-structures">Data Structures</a></li>
<li><a href="#database">Database</a></li>
<li><a href="#database-drivers">Database Drivers</a></li>
<li><a href="#date-and-time">Date and Time</a></li>
<li><a href="#distributed-systems">Distributed Systems</a></li>
<li><a href="#email">Email</a></li>
<li><a href="#embeddable-scripting-languages">Embeddable Scripting Languages</a></li>
<li><a href="#error-handling">Error Handling</a></li>
<li><a href="#files">Files</a></li>
<li><a href="#financial">Financial</a></li>
<li><a href="#forms">Forms</a></li>
<li><a href="#functional">Functional</a></li>
<li><a href="#game-development">Game Development</a></li>
<li><a href="#generation-and-generics">Generation and Generics</a></li>
<li><a href="#geographic">Geographic</a></li>
<li><a href="#go-compilers">Go Compilers</a></li>
<li><a href="#goroutines">Goroutines</a></li>
<li><a href="#gui">GUI</a></li>
<li><a href="#hardware">Hardware</a></li>
<li><a href="#images">Images</a></li>
<li><a href="#iot-internet-of-things">IoT</a></li>
<li><a href="#job-scheduler">Job Scheduler</a></li>
<li><a href="#json">JSON</a></li>
<li><a href="#logging">Logging</a></li>
<li><a href="#machine-learning">Machine Learning</a></li>
<li><a href="#messaging">Messaging</a></li>
<li><a href="#microsoft-office">Microsoft Office</a>

<ul>
<li><a href="#microsoft-excel">Microsoft Excel</a></li>
</ul></li>
<li><a href="#miscellaneous">Miscellaneous</a>

<ul>
<li><a href="#dependency-injection">Dependency Injection</a></li>
<li><a href="#strings">Strings</a></li>
</ul></li>
<li><a href="#natural-language-processing">Natural Language Processing</a></li>
<li><a href="#networking">Networking</a>

<ul>
<li><a href="#http-clients">HTTP Clients</a></li>
</ul></li>
<li><a href="#opengl">OpenGL</a></li>
<li><a href="#orm">ORM</a></li>
<li><a href="#package-management">Package Management</a></li>
<li><a href="#query-language">Query Language</a></li>
<li><a href="#resource-embedding">Resource Embedding</a></li>
<li><a href="#science-and-data-analysis">Science and Data Analysis</a></li>
<li><a href="#security">Security</a></li>
<li><a href="#serialization">Serialization</a></li>
<li><a href="#template-engines">Template Engines</a></li>
<li><a href="#testing">Testing</a></li>
<li><a href="#text-processing">Text Processing</a></li>
<li><a href="#third-party-apis">Third-party APIs</a></li>
<li><a href="#utilities">Utilities</a></li>
<li><a href="#uuid">UUID</a></li>
<li><a href="#validation">Validation</a></li>
<li><a href="#version-control">Version Control</a></li>
<li><a href="#video">Video</a></li>
<li><a href="#web-frameworks">Web Frameworks</a>

<ul>
<li><a href="#middlewares">Middlewares</a>

<ul>
<li><a href="#actual-middlewares">Actual middlewares</a></li>
<li><a href="#libraries-for-creating-http-middlewares">Libraries for creating HTTP middlewares</a></li>
</ul></li>
<li><a href="#routers">Routers</a></li>
</ul></li>
<li><a href="#windows">Windows</a></li>
<li><a href="#xml">XML</a></li>
</ul></li>

<li><p><a href="#tools">Tools</a></p>

<ul>
<li><a href="#code-analysis">Code Analysis</a></li>
<li><a href="#editor-plugins">Editor Plugins</a></li>
<li><a href="#go-generate-tools">Go Generate Tools</a></li>
<li><a href="#go-tools">Go Tools</a></li>
<li><a href="#software-packages">Software Packages</a>

<ul>
<li><a href="#devops-tools">DevOps Tools</a></li>
<li><a href="#other-software">Other Software</a></li>
</ul></li>
</ul></li>

<li><p><a href="#server-applications">Server Applications</a></p></li>

<li><p><a href="#resources">Resources</a></p>

<ul>
<li><a href="#benchmarks">Benchmarks</a></li>
<li><a href="#conferences">Conferences</a></li>
<li><a href="#e-books">E-Books</a></li>
<li><a href="#gophers">Gophers</a></li>
<li><a href="#meetups">Meetups</a></li>
<li><a href="#twitter">Twitter</a></li>
<li><a href="#websites">Websites</a>

<ul>
<li><a href="#tutorials">Tutorials</a></li>
</ul></li>
</ul></li>
</ul>

<h2>Audio and Music</h2>

<p><em>Libraries for manipulating audio.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/algoGuy/EasyMIDI">EasyMIDI</a></td>
<td>EasyMidi is a simple and reliable library for working with standard midi file (SMF).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/eaburns/flac">flac</a></td>
<td>No-frills native Go FLAC decoder that decodes FLAC files into byte slices.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mewkiz/flac">flac</a></td>
<td>Native Go FLAC encoder/decoder with support for FLAC streams.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Comcast/gaad">gaad</a></td>
<td>Native Go AAC bitstream parser.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/krig/go-sox">go-sox</a></td>
<td>libsox bindings for go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zhulik/go_mediainfo">go_mediainfo</a></td>
<td>libmediainfo bindings for go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dh1tw/gosamplerate">gosamplerate</a></td>
<td>libsamplerate bindings for go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bogem/id3v2">id3v2</a></td>
<td>Fast and stable ID3 parsing and writing library for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gen2brain/malgo">malgo</a></td>
<td>Mini audio library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tosone/minimp3">minimp3</a></td>
<td>Lightweight MP3 decoder library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-mix/mix">mix</a></td>
<td>Sequence-based Go-native audio mixer for music apps.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tcolgate/mp3">mp3</a></td>
<td>Native Go MP3 decoder.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-music-theory/music-theory">music-theory</a></td>
<td>Music theory models in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gordonklaus/portaudio">PortAudio</a></td>
<td>Go bindings for the PortAudio audio I/O library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rakyll/portmidi">portmidi</a></td>
<td>Go bindings for PortMidi.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/wtolson/go-taglib">taglib</a></td>
<td>Go bindings for taglib.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mccoyst/vorbis">vorbis</a></td>
<td>&ldquo;Native&rdquo; Go Vorbis decoder (uses CGO, but has no dependencies).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mdlayher/waveform">waveform</a></td>
<td>Go package capable of generating waveform images from audio streams.</td>
</tr>
</tbody>
</table>

<h2>Authentication and OAuth</h2>

<p><em>Libraries for implementing authentications schemes.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/volatiletech/authboss">authboss</a></td>
<td>Modular authentication system for the web. It tries to remove as much boilerplate and &ldquo;hard things&rdquo; as possible so that each time you start a new web project in Go, you can plug it in, configure, and start building your app without having to build an authentication system each time.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hako/branca">branca</a></td>
<td>Golang implementation of Branca Tokens.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hsluoyz/casbin">casbin</a></td>
<td>Authorization library that supports access control models like ACL, RBAC, ABAC.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mengzhuo/cookiestxt">cookiestxt</a></td>
<td>provides parser of cookies.txt file format.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/square/go-jose">go-jose</a></td>
<td>Fairly complete implementation of the JOSE working group&rsquo;s JSON Web Token, JSON Web Signatures, and JSON Web Encryption specs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/RichardKnop/go-oauth2-server">go-oauth2-server</a></td>
<td>Standalone, specification-compliant,  OAuth2 server written in Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dghubble/gologin">gologin</a></td>
<td>chainable handlers for login with OAuth1 and OAuth2 authentication providers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mikespook/gorbac">gorbac</a></td>
<td>provides a lightweight role-based access control (RBAC) implementation in Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/markbates/goth">goth</a></td>
<td>provides a simple, clean, and idiomatic way to use OAuth and OAuth2. Handles multiple providers out of the box.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/goji/httpauth">httpauth</a></td>
<td>HTTP Authentication middleware.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/robbert229/jwt">jwt</a></td>
<td>Clean and easy to use implementation of JSON Web Tokens (JWT).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pascaldekloe/jwt">jwt</a></td>
<td>Lightweight JSON Web Token (JWT) library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/adam-hanna/jwt-auth">jwt-auth</a></td>
<td>JWT middleware for Golang http servers with many configuration options.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dgrijalva/jwt-go">jwt-go</a></td>
<td>Golang implementation of JSON Web Tokens (JWT).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tarent/loginsrv">loginsrv</a></td>
<td>JWT login microservice with plugable backends such as OAuth2 (Github), htpasswd, osiam.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/golang/oauth2">oauth2</a></td>
<td>Successor of goauth2. Generic OAuth 2.0 package that comes with JWT, Google APIs, Compute Engine and App Engine support.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/openshift/osin">osin</a></td>
<td>Golang OAuth2 server library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/o1egl/paseto">paseto</a></td>
<td>Golang implementation of Platform-Agnostic Security Tokens (PASETO).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xyproto/permissions2">permissions2</a></td>
<td>Library for keeping track of users, login states and permissions. Uses secure cookies and bcrypt.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zpatrick/rbac">rbac</a></td>
<td>Minimalistic RBAC package for Go applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/chmike/securecookie">securecookie</a></td>
<td>Efficient secure cookie encoding/decoding.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/icza/session">session</a></td>
<td>Go session management for web servers (including support for Google App Engine - GAE).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/f0rmiga/sessiongate-go">sessiongate-go</a></td>
<td>Go session management using the SessionGate Redis module.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/adam-hanna/sessions">sessions</a></td>
<td>Dead simple, highly performant, highly customizable sessions service for go http servers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sashka/signedvalue">signedvalue</a></td>
<td>Signed and timestamped strings compatible with <a href="https://github.com/tornadoweb/tornado">Tornado&rsquo;s</a> <code>create_signed_value</code>, <code>decode_signed_value</code>, and therefore <code>set_secure_cookie</code> and <code>get_secure_cookie</code>.</td>
</tr>
</tbody>
</table>

<h2>Bot Building</h2>

<p><em>Libraries for building and working with bots.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-chat-bot/bot">go-chat-bot</a></td>
<td>IRC, Slack &amp; Telegram bot written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/oklahomer/go-sarah">go-sarah</a></td>
<td>Framework to build bot for desired chat services including LINE, Slack, Gitter and more.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/olebedev/go-tgbot">go-tgbot</a></td>
<td>Pure Golang Telegram Bot API wrapper, generated from swagger file, session-based router and middleware.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/saniales/golang-crypto-trading-bot">Golang CryptoTrading Bot</a></td>
<td>A golang implementation of a console-based trading bot for cryptocurrency exchanges.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nikepan/govkbot">govkbot</a></td>
<td>Simple Go <a href="https://vk.com">VK</a> bot library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sbstjn/hanu">hanu</a></td>
<td>Framework for writing Slack bots.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zhulik/margelet">margelet</a></td>
<td>Framework for building Telegram bots.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/onrik/micha">micha</a></td>
<td>Go Library for Telegram bot api.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shomali11/slacker">slacker</a></td>
<td>Easy to use framework to create Slack bots.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yanzay/tbot">tbot</a></td>
<td>Telegram bot server with API similar to net/http.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tucnak/telebot">telebot</a></td>
<td>Telegram bot framework written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Syfaro/telegram-bot-api">telegram-bot-api</a></td>
<td>Simple and clean Telegram bot client.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kyleterry/tenyks">Tenyks</a></td>
<td>Service oriented IRC bot using Redis and JSON for messaging.</td>
</tr>
</tbody>
</table>

<h2>Command Line</h2>

<h3>Standard CLI</h3>

<p><em>Libraries for building standard or basic Command Line applications.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/akamensky/argparse">argparse</a></td>
<td>Command line argument parser inspired by Python&rsquo;s argparse module.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cosiner/argv">argv</a></td>
<td>Go library to split command line string as arguments array using the bash syntax.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mkideal/cli">cli</a></td>
<td>Feature-rich and easy to use command-line package based on golang struct tags.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/teris-io/cli">cli</a></td>
<td>Simple and complete API for building command line interfaces in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tcnksm/gcli">cli-init</a></td>
<td>The easy way to start building Golang command line applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="http://github.com/tucnak/climax">climax</a></td>
<td>Alternative CLI with &ldquo;human face&rdquo;, in spirit of Go command.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/spf13/cobra">cobra</a></td>
<td>Commander for modern Go CLI interactions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jaffee/commandeer">commandeer</a></td>
<td>Dev-friendly CLI apps: sets up flags, defaults, and usage based on struct fields and tags.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/posener/complete">complete</a></td>
<td>Write bash completions in Go + Go command bash completion.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/docopt/docopt.go">docopt.go</a></td>
<td>Command-line arguments parser that will make you smile.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/codingconcepts/env">env</a></td>
<td>Tag-based environment configuration for structs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cosiner/flag">flag</a></td>
<td>Simple but powerful command line option parsing library for Go supporting subcommand.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/integrii/flaggy">flaggy</a></td>
<td>A robust and idiomatic flags package with excellent subcommand support.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sgreben/flagvar">flagvar</a></td>
<td>A collection of flag argument types for Go&rsquo;s standard <code>flag</code> package.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/alexflint/go-arg">go-arg</a></td>
<td>Struct-based argument parsing in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yitsushi/go-commander">go-commander</a></td>
<td>Go library to simplify CLI workflow.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jessevdk/go-flags">go-flags</a></td>
<td>go command line option parser.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/devfacet/gocmd">gocmd</a></td>
<td>Go library for building command line applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hidevopsio/hiboot/tree/master/pkg/app/cli">hiboot cli</a></td>
<td>cli application framework with auto configuration and dependency injection.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/alecthomas/kingpin">kingpin</a></td>
<td>Command line and flag parser supporting sub commands.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/peterh/liner">liner</a></td>
<td>Go readline-like library for command-line interfaces.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mitchellh/cli">mitchellh/cli</a></td>
<td>Go library for implementing command-line interfaces.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jawher/mow.cli">mow.cli</a></td>
<td>Go library for building CLI applications with sophisticated flag and argument parsing and validation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/spf13/pflag">pflag</a></td>
<td>Drop-in replacement for Go&rsquo;s flag package, implementing POSIX/GNU-style &ndash;flags.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/chzyer/readline">readline</a></td>
<td>Pure golang implementation that provides most features in GNU-Readline under MIT license.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Zaba505/sand">sand</a></td>
<td>Simple API for creating interpreters and so much more.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/octago/sflags">sflags</a></td>
<td>Struct based flags generator for flag, urfave/cli, pflag, cobra, kingpin and other libraries.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/antham/strumt">strumt</a></td>
<td>Library to create prompt chain.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ukautz/clif">ukautz/clif</a></td>
<td>Small command line interface framework.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/urfave/cli">urfave/cli</a></td>
<td>Simple, fast, and fun package for building command line apps in Go (formerly codegangsta/cli).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dixonwille/wlog">wlog</a></td>
<td>Simple logging interface that supports cross-platform color and concurrency.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dixonwille/wmenu">wmenu</a></td>
<td>Easy to use menu structure for cli applications that prompts users to make choices.</td>
</tr>
</tbody>
</table>

<h3>Advanced Console UIs</h3>

<p><em>Libraries for building Console Applications and Console User Interfaces.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/logrusorgru/aurora">aurora</a></td>
<td>ANSI terminal colors that supports fmt.Printf/Sprintf.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mingrammer/cfmt">cfmt</a></td>
<td>Contextual fmt inspired by bootstrap color classes.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ttacon/chalk">chalk</a></td>
<td>Intuitive package for prettifying terminal/console output.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fatih/color">color</a></td>
<td>Versatile package for colored terminal output.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/TreyBastian/colourize">colourize</a></td>
<td>Go library for ANSI colour text in terminals.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/wzshiming/ctc">ctc</a></td>
<td>The non-invasive cross-platform terminal color library does not need to modify the Print method.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/workanator/go-ataman">go-ataman</a></td>
<td>Go library for rendering ANSI colored text templates in terminals.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mattn/go-colorable">go-colorable</a></td>
<td>Colorable writer for windows.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/daviddengcn/go-colortext">go-colortext</a></td>
<td>Go library for color output in terminals.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mattn/go-isatty">go-isatty</a></td>
<td>isatty for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/c-bata/go-prompt">go-prompt</a></td>
<td>Library for building a powerful interactive prompt, inspired by <a href="https://github.com/jonathanslenders/python-prompt-toolkit">python-prompt-toolkit</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jroimartin/gocui">gocui</a></td>
<td>Minimalist Go library aimed at creating Console User Interfaces.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/labstack/gommon/tree/master/color">gommon/color</a></td>
<td>Style terminal text.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gookit/color">gookit/color</a></td>
<td>Terminal color rendering tool library, support 16 colors, 256 colors, RGB color rendering output, compatible with Windows.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/vbauerster/mpb">mpb</a></td>
<td>Multi progress bar for terminal applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/schollz/progressbar">progressbar</a></td>
<td>Basic thread-safe progress bar that works in every OS.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/alexeyco/simpletable">simpletable</a></td>
<td>Simple tables in terminal with Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/InVisionApp/tabular">tabular</a></td>
<td>Print ASCII tables from command line utilities without the need to pass large sets of data to the API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nsf/termbox-go">termbox-go</a></td>
<td>Termbox is a library for creating cross-platform text-based interfaces.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mum4k/termdash">termdash</a></td>
<td>Go terminal dashboard based on <strong>termbox-go</strong> and inspired by <a href="https://github.com/gizak/termui">termui</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/apcera/termtables">termtables</a></td>
<td>Go port of the Ruby library <a href="https://github.com/tj/terminal-table">terminal-tables</a> for simple ASCII table generation as well as providing markdown and HTML output.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gizak/termui">termui</a></td>
<td>Go terminal dashboard based on <strong>termbox-go</strong> and inspired by <a href="https://github.com/yaronn/blessed-contrib">blessed-contrib</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gosuri/uilive">uilive</a></td>
<td>Library for updating terminal output in realtime.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gosuri/uiprogress">uiprogress</a></td>
<td>Flexible library to render progress bars in terminal applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gosuri/uitable">uitable</a></td>
<td>Library to improve readability in terminal apps using tabular data.</td>
</tr>
</tbody>
</table>

<h2>Configuration</h2>

<p><em>Libraries for configuration parsing.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/olebedev/config">config</a></td>
<td>JSON or YAML configuration wrapper with environment variables and flags parsing.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/paked/configure">configure</a></td>
<td>Provides configuration through multiple sources, including JSON, flags and environment variables.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/heetch/confita">confita</a></td>
<td>Load configuration in cascade from multiple backends into a struct.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/miracl/conflate">conflate</a></td>
<td>Library/tool to merge multiple JSON/YAML/TOML files from arbitrary URLs, validation against a JSON schema, and application of default values defined in the schema.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/caarlos0/env">env</a></td>
<td>Parse environment variables to Go structs (with defaults).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tomazk/envcfg">envcfg</a></td>
<td>Un-marshaling environment variables to Go structs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ian-kent/envconf">envconf</a></td>
<td>Configuration from environment.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/vrischmann/envconfig">envconfig</a></td>
<td>Read your configuration from environment variables.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/antham/envh">envh</a></td>
<td>Helpers to manage environment variables.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-gcfg/gcfg">gcfg</a></td>
<td>read INI-style configuration files into Go structs; supports user-defined types and subsections.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ufoscout/go-up">go-up</a></td>
<td>A simple configuration library with recursive placeholders resolution and no magic.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/crgimenes/goConfig">goConfig</a></td>
<td>Parses a struct as input and populates the fields of this struct with parameters from command line, environment variables and configuration file.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/joho/godotenv">godotenv</a></td>
<td>Go port of Ruby&rsquo;s dotenv library (Loads environment variables from <code>.env</code>).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ian-kent/gofigure">gofigure</a></td>
<td>Go application configuration made easy.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/One-com/gone/tree/master/jconf">gone/jconf</a></td>
<td>Modular JSON configuration. Keep you config structs along with the code they configure and delegate parsing to submodules without sacrificing full config serialization.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gookit/config">gookit/config</a></td>
<td>application config manage(load,get,set). support JSON, YAML, TOML, INI, HCL. multi file load, data override merge.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hjson/hjson-go">hjson</a></td>
<td>Human JSON, a configuration file format for humans. Relaxed syntax, fewer mistakes, more comments.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/schachmat/ingo">ingo</a></td>
<td>Flags persisted in an ini-like config file.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-ini/ini">ini</a></td>
<td>Go package to read and write INI files.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/joshbetz/config">joshbetz/config</a></td>
<td>Small configuration library for Go that parses environment variables, JSON files, and reloads automatically on SIGHUP.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kelseyhightower/envconfig">kelseyhightower/envconfig</a></td>
<td>Go library for managing configuration data from environment variables.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sasbury/mini">mini</a></td>
<td>Golang package for parsing ini-style configuration files.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/oblq/sprbox">sprbox</a></td>
<td>Build-environment aware toolbox factory and agnostic config parser (YAML, TOML, JSON and Environment vars).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tucnak/store">store</a></td>
<td>Lightweight configuration manager for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/spf13/viper">viper</a></td>
<td>Go configuration with fangs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/OpenPeeDeeP/xdg">xdg</a></td>
<td>Cross platform package that follows the <a href="https://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html">XDG Standard</a>.</td>
</tr>
</tbody>
</table>

<h2>Continuous Integration</h2>

<p><em>Tools for help with continuous integration.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/drone/drone">drone</a></td>
<td>Drone is a Continuous Integration platform built on Docker, written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/duck8823/duci">duci</a></td>
<td>A simple ci server no needs domain specific languages.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nikogura/gomason">gomason</a></td>
<td>Test, Build, Sign, and Publish your go binaries from a clean workspace.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mattn/goveralls">goveralls</a></td>
<td>Go integration for Coveralls.io continuous code coverage tracking system.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-playground/overalls">overalls</a></td>
<td>Multi-Package go project coverprofile for tools like goveralls.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/LawrenceWoodman/roveralls">roveralls</a></td>
<td>Recursive coverage testing tool.</td>
</tr>
</tbody>
</table>

<h2>CSS Preprocessors</h2>

<p><em>Libraries for preprocessing CSS files.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yosssi/gcss">gcss</a></td>
<td>Pure Go CSS Preprocessor.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/wellington/go-libsass">go-libsass</a></td>
<td>Go wrapper to the 100% Sass compatible libsass project.</td>
</tr>
</tbody>
</table>

<h2>Data Structures</h2>

<p><em>Generic datastructures and algorithms in Go.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shady831213/algorithms">algorithms</a></td>
<td>Algorithms and data structures.CLRS study.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zhuangsirui/binpacker">binpacker</a></td>
<td>Binary packer and unpacker helps user build custom binary stream.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yourbasic/bit">bit</a></td>
<td>Golang set data structure with bonus bit-twiddling functions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/willf/bitset">bitset</a></td>
<td>Go package implementing bitsets.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zhenjl/bloom">bloom</a></td>
<td>Bloom filters implemented in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yourbasic/bloom">bloom</a></td>
<td>Golang Bloom filter implementation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tylertreat/BoomFilters">boomfilters</a></td>
<td>Probabilistic data structures for processing continuous, unbounded streams.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/free/concurrent-writer">concurrent-writer</a></td>
<td>Highly concurrent drop-in replacement for <code>bufio.Writer</code>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/InVisionApp/conjungo">conjungo</a></td>
<td>A small, powerful and flexible merge library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/seiflotfy/count-min-log">count-min-log</a></td>
<td>Go implementation Count-Min-Log sketch: Approximately counting with approximate counters (Like Count-Min sketch but using less memory).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/seiflotfy/cuckoofilter">cuckoofilter</a></td>
<td>Cuckoo filter: a good alternative to a counting bloom filter implemented in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gammazero/deque">deque</a></td>
<td>Fast ring-buffer deque (double-ended queue).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zhenjl/encoding">encoding</a></td>
<td>Integer Compression Libraries for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/plar/go-adaptive-radix-tree">go-adaptive-radix-tree</a></td>
<td>Go implementation of Adaptive Radix Tree.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Workiva/go-datastructures">go-datastructures</a></td>
<td>Collection of useful, performant, and thread-safe data structures.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/amallia/go-ef">go-ef</a></td>
<td>A Go implementation of the Elias-Fano encoding.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hailocab/go-geoindex">go-geoindex</a></td>
<td>In-memory geo index.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/OrlovEvgeny/go-mcache">go-mcache</a></td>
<td>Fast in-memory key:value store/cache library. Pointer caches.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aurelien-rainone/go-rquad">go-rquad</a></td>
<td>Region quadtrees with efficient point location and neighbour finding.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/enriquebris/goconcurrentqueue">goconcurrentqueue</a></td>
<td>Concurrent FIFO queue.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/emirpasic/gods">gods</a></td>
<td>Go Data Structures. Containers, Sets, Lists, Stacks, Maps, BidiMaps, Trees, HashSet etc.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/deckarep/golang-set">golang-set</a></td>
<td>Thread-Safe and Non-Thread-Safe high-performance sets for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zoumo/goset">goset</a></td>
<td>A useful Set collection implementation for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ryszard/goskiplist">goskiplist</a></td>
<td>Skip list implementation in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kniren/gota">gota</a></td>
<td>Implementation of dataframes, series, and data wrangling methods for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/google/hilbert">hilbert</a></td>
<td>Go package for mapping values to and from space-filling curves, such as Hilbert and Peano curves.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/axiomhq/hyperloglog">hyperloglog</a></td>
<td>HyperLogLog implementation with Sparse, LogLog-Beta bias correction and TailCut space reduction.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/agext/levenshtein">levenshtein</a></td>
<td>Levenshtein distance and similarity metrics with customizable edit costs and Winkler-like bonus for common prefix.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/agnivade/levenshtein">levenshtein</a></td>
<td>Implementation to calculate levenshtein distance in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/smartystreets/mafsa">mafsa</a></td>
<td>MA-FSA implementation with Minimal Perfect Hashing.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cbergoon/merkletree">merkletree</a></td>
<td>Implementation of a merkle tree providing an efficient and secure verification of the contents of data structures.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/BlackRabbitt/mspm">mspm</a></td>
<td>Multi-String Pattern Matching Algorithm for information retrieval.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hyfather/pipeline">pipeline</a></td>
<td>An implementation of pipelines with fan-in and fan-out.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/RoaringBitmap/roaring">roaring</a></td>
<td>Go package implementing compressed bitsets.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/StudioSol/set">set</a></td>
<td>Simple set data structure implementation in Go using LinkedHashMap.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/MauriceGit/skiplist">skiplist</a></td>
<td>Very fast Go Skiplist implementation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gansidui/skiplist">skiplist</a></td>
<td>Skiplist implementation in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/derekparker/trie">trie</a></td>
<td>Trie implementation in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/diegobernardes/ttlcache">ttlcache</a></td>
<td>In-memory LRU string-interface{} map with expiration for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/willf/bloom">willf/bloom</a></td>
<td>Go package implementing Bloom filters.</td>
</tr>
</tbody>
</table>

<h2>Database</h2>

<p><em>Databases implemented in Go.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dgraph-io/badger">badger</a></td>
<td>Fast key-value store in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/allegro/bigcache">BigCache</a></td>
<td>Efficient key/value cache for gigabytes of data.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/boltdb/bolt">bolt</a></td>
<td>Low-level key/value database for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tidwall/buntdb">buntdb</a></td>
<td>Fast, embeddable, in-memory key/value database for Go with custom indexing and spatial support.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/muesli/cache2go">cache2go</a></td>
<td>In-memory key:value cache which supports automatic invalidation based on timeouts.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/oaStuff/clusteredBigCache">clusteredBigCache</a></td>
<td>BigCache with clustering support and individual item expiration.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cockroachdb/cockroach">cockroach</a></td>
<td>Scalable, Geo-Replicated, Transactional Datastore.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/codingsince1985/couchcache">couchcache</a></td>
<td>RESTful caching micro-service backed by Couchbase server.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/CovenantSQL/CovenantSQL">CovenantSQL</a></td>
<td>CovenantSQL is a SQL database on blockchain.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dgraph-io/dgraph">dgraph</a></td>
<td>Scalable, Distributed, Low Latency, High Throughput Graph Database.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/peterbourgon/diskv">diskv</a></td>
<td>Home-grown disk-backed key-value store.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/krotik/eliasdb">eliasdb</a></td>
<td>Dependency-free, transactional graph database with REST API, phrase search and SQL-like query language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/VictoriaMetrics/fastcache">fastcache</a></td>
<td>fast thread-safe inmemory cache for big number of entries. Minimizes GC overhead.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bluele/gcache">GCache</a></td>
<td>Cache library with support for expirable Cache, LFU, LRU and ARC.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pmylund/go-cache">go-cache</a></td>
<td>In-memory key:value store/cache (similar to Memcached) library for Go, suitable for single-machine applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/syndtr/goleveldb">goleveldb</a></td>
<td>Implementation of the <a href="https://github.com/google/leveldb">LevelDB</a> key/value database in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kapitan-k/gorocksdb">gorocksdb</a></td>
<td>Gorocksdb is a wrapper for <a href="https://rocksdb.org">RocksDB</a> written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/golang/groupcache">groupcache</a></td>
<td>Groupcache is a caching and cache-filling library, intended as a replacement for memcached in many cases.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/influxdb/influxdb">influxdb</a></td>
<td>Scalable datastore for metrics, events, and real-time analytics.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/siddontang/ledisdb">ledisdb</a></td>
<td>Ledisdb is a high performance NoSQL like Redis based on LevelDB.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jmhodges/levigo">levigo</a></td>
<td>Levigo is a Go wrapper for LevelDB.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/couchbase/moss">moss</a></td>
<td>Moss is a simple LSM key-value storage engine written in 100% Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fern4lvarez/piladb">piladb</a></td>
<td>Lightweight RESTful database engine based on stack data structures.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/prometheus/prometheus">prometheus</a></td>
<td>Monitoring system and time series database.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/recoilme/pudge">pudge</a></td>
<td>Fast and simple  key/value store written using Go&rsquo;s standard library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rqlite/rqlite">rqlite</a></td>
<td>The lightweight, distributed, relational database built on SQLite.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nanobox-io/golang-scribble">Scribble</a></td>
<td>Tiny flat file JSON store.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/recoilme/slowpoke">slowpoke</a></td>
<td>Key-value store with persistence.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rafaeljesus/tempdb">tempdb</a></td>
<td>Key-value store for temporary items.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pingcap/tidb">tidb</a></td>
<td>TiDB is a distributed SQL database. Inspired by the design of Google F1.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/HouzuoGuo/tiedot">tiedot</a></td>
<td>Your NoSQL database powered by Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/chrislusf/vasto">Vasto</a></td>
<td>A distributed high-performance key-value store. On Disk. Eventual consistent. HA. Able to grow or shrink without service interruption.</td>
</tr>
</tbody>
</table>
<p><em>Database schema migration.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/GuiaBolso/darwin">darwin</a></td>
<td>Database schema evolution library for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/RichardKnop/go-fixtures">go-fixtures</a></td>
<td>Django style fixtures for Golang&rsquo;s excellent built-in database/sql library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/robinjoseph08/go-pg-migrations">go-pg-migrations</a></td>
<td>A Go package to help write migrations with go-pg/pg.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/emvicom/gondolier">gondolier</a></td>
<td>Gondolier is a library to auto migrate database schemas using structs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/steinbacher/goose">goose</a></td>
<td>Database migration tool. You can manage your database&rsquo;s evolution by creating incremental SQL or Go scripts.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-gormigrate/gormigrate">gormigrate</a></td>
<td>Database schema migration helper for Gorm ORM.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/golang-migrate/migrate">migrate</a></td>
<td>Database migrations. CLI and Golang library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pravasan/pravasan">pravasan</a></td>
<td>Simple Migration tool - currently for MySQL but planning to soon support Postgres, SQLite, MongoDB, etc.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gobuffalo/pop/tree/master/soda">soda</a></td>
<td>Database migration, creation, ORM, etc&hellip; for MySQL, PostgreSQL, and SQLite.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rubenv/sql-migrate">sql-migrate</a></td>
<td>Database migration tool. Allows embedding migrations into the application using go-bindata.</td>
</tr>
</tbody>
</table>
<p><em>Database tools.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Vertamedia/chproxy">chproxy</a></td>
<td>HTTP proxy for ClickHouse database.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nikepan/clickhouse-bulk">clickhouse-bulk</a></td>
<td>Collects small insterts and sends big requests to ClickHouse servers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sj14/dbbench">dbbench</a></td>
<td>Database benchmarking tool with support for several databases and scripts.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/siddontang/go-mysql">go-mysql</a></td>
<td>Go toolset to handle MySQL protocol and replication.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/siddontang/go-mysql-elasticsearch">go-mysql-elasticsearch</a></td>
<td>Sync your MySQL data into Elasticsearch automatically.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/flike/kingshard">kingshard</a></td>
<td>kingshard is a high performance proxy for MySQL powered by Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/2tvenom/myreplication">myreplication</a></td>
<td>MySql binary log replication listener. Supports statement and row based replication.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/knocknote/octillery">octillery</a></td>
<td>Go package for sharding databases ( Supports every ORM or raw SQL ).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/github/orchestrator">orchestrator</a></td>
<td>MySQL replication topology manager &amp; visualizer.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sosedoff/pgweb">pgweb</a></td>
<td>Web-based PostgreSQL database browser.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hexdigest/prep">prep</a></td>
<td>Use prepared SQL statements without changing your code.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nuveo/prest">pREST</a></td>
<td>Serve a RESTful API from any PostgreSQL database.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/andizzle/rwdb">rwdb</a></td>
<td>rwdb provides read replica capability for multiple database servers setup.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/youtube/vitess">vitess</a></td>
<td>vitess provides servers and tools which facilitate scaling of MySQL databases for large scale web services.</td>
</tr>
</tbody>
</table>
<p><em>SQL query builder, libraries for building and using SQL.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gchaincl/dotsql">Dotsql</a></td>
<td>Go library that helps you keep sql files in one place and use them with ease.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/didi/gendry">gendry</a></td>
<td>Non-invasive SQL builder and powerful data binder.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xujiajun/godbal">godbal</a></td>
<td>Database Abstraction Layer (dbal) for go. Support SQL builder and get result easily.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/doug-martin/goqu">goqu</a></td>
<td>Idiomatic SQL builder and query library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/galeone/igor">igor</a></td>
<td>Abstraction layer for PostgreSQL that supports advanced functionality and uses gorm-like syntax.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pupizoid/ormlite">ormlite</a></td>
<td>Lightweight package containing some ORM-like features and helpers for sqlite databases.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-ozzo/ozzo-dbx">ozzo-dbx</a></td>
<td>Powerful data retrieval methods as well as DB-agnostic query building capabilities.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/variadico/scaneo">scaneo</a></td>
<td>Generate Go code to convert database rows into arbitrary structs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/elgris/sqrl">sqrl</a></td>
<td>SQL query builder, fork of Squirrel with improved performance.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Masterminds/squirrel">Squirrel</a></td>
<td>Go library that helps you build SQL queries.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/knq/xo">xo</a></td>
<td>Generate idiomatic Go code for databases based on existing schema definitions or custom queries supporting PostgreSQL, MySQL, SQLite, Oracle, and Microsoft SQL Server.</td>
</tr>
</tbody>
</table>

<h2>Database Drivers</h2>

<p><em>Libraries for connecting and operating databases.</em></p>

<ul>
<li>Relational Databases</li>
</ul>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/apache/calcite-avatica-go">avatica</a></td>
<td>Apache Avatica/Phoenix SQL driver for database/sql.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/viant/bgc">bgc</a></td>
<td>Datastore Connectivity for BigQuery for go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nakagami/firebirdsql">firebirdsql</a></td>
<td>Firebird RDBMS SQL driver for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mattn/go-adodb">go-adodb</a></td>
<td>Microsoft ActiveX Object DataBase driver for go that uses database/sql.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/denisenkom/go-mssqldb">go-mssqldb</a></td>
<td>Microsoft MSSQL driver for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mattn/go-oci8">go-oci8</a></td>
<td>Oracle driver for go that uses database/sql.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-sql-driver/mysql">go-sql-driver/mysql</a></td>
<td>MySQL driver for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mattn/go-sqlite3">go-sqlite3</a></td>
<td>SQLite3 driver for go that uses database/sql.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/minus5/gofreetds">gofreetds</a></td>
<td>Microsoft MSSQL driver. Go wrapper over <a href="http://www.freetds.org">FreeTDS</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-goracle/goracle">goracle</a></td>
<td>Oracle driver for Go, using the ODPI-C driver.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jackc/pgx">pgx</a></td>
<td>PostgreSQL driver supporting features beyond those exposed by database/sql.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/lib/pq">pq</a></td>
<td>Pure Go Postgres driver for database/sql.</td>
</tr>
</tbody>
</table>

<ul>
<li>NoSQL Databases</li>
</ul>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aerospike/aerospike-client-go">aerospike-client-go</a></td>
<td>Aerospike client in Go language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/solher/arangolite">arangolite</a></td>
<td>Lightweight golang driver for ArangoDB.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/viant/asc">asc</a></td>
<td>Datastore Connectivity for Aerospike for go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/underarmour/dynago">dynago</a></td>
<td>Dynago is a principle of least surprise client for DynamoDB.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/couchbase/goforestdb">forestdb</a></td>
<td>Go bindings for ForestDB.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/couchbase/go-couchbase">go-couchbase</a></td>
<td>Couchbase client in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fjl/go-couchdb">go-couchdb</a></td>
<td>Yet another CouchDB HTTP API wrapper for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nitishm/go-rejson">go-rejson</a></td>
<td>Golang client for redislabs&rsquo; ReJSON module using Redigo golang client. Store and manipulate structs as JSON objects in redis with ease.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/couchbase/gocb">gocb</a></td>
<td>Official Couchbase Go SDK.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://gocql.github.io">gocql</a></td>
<td>Go language driver for Apache Cassandra.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/defcronyke/godscache">godscache</a></td>
<td>A wrapper for the Google Cloud Platform Go Datastore package that adds caching using memcached.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bradfitz/gomemcache/">gomemcache</a></td>
<td>memcache client library for the Go programming language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dancannon/gorethink">gorethink</a></td>
<td>Go language driver for RethinkDB.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zegl/goriak">goriak</a></td>
<td>Go language driver for Riak KV.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/globalsign/mgo">mgo</a></td>
<td>MongoDB driver for the Go language that implements a rich and well tested selection of features under a very simple API following standard Go idioms.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mongodb/mongo-go-driver">mongo-go-driver</a></td>
<td>Official MongoDB driver for the Go language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cihangir/neo4j">neo4j</a></td>
<td>Neo4j Rest API Bindings for Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/davemeehan/Neo4j-GO">Neo4j-GO</a></td>
<td>Neo4j REST Client in golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jmcvetta/neoism">neoism</a></td>
<td>Neo4j client for Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gomodule/redigo">redigo</a></td>
<td>Redigo is a Go client for the Redis database.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-redis/redis">redis</a></td>
<td>Redis client for Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bsm/redeo">redis</a></td>
<td>Redis-protocol compatible TCP servers/services.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shomali11/xredis">xredis</a></td>
<td>Typesafe, customizable, clean &amp; easy to use Redis client.</td>
</tr>
</tbody>
</table>

<ul>
<li>Search and Analytic Databases.</li>
</ul>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/blevesearch/bleve">bleve</a></td>
<td>Modern text indexing library for go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/olivere/elastic">elastic</a></td>
<td>Elasticsearch client for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cch123/elasticsql">elasticsql</a></td>
<td>Convert sql to elasticsearch dsl in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mattbaird/elastigo">elastigo</a></td>
<td>Elasticsearch client library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/OwnLocal/goes">goes</a></td>
<td>Library to interact with Elasticsearch.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-ego/riot">riot</a></td>
<td>Go Open Source, Distributed, Simple and efficient Search Engine.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/seiflotfy/skizze">skizze</a></td>
<td>probabilistic data-structures service and storage.</td>
</tr>
</tbody>
</table>

<ul>
<li>Multiple Backends.</li>
</ul>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fabiorphp/cachego">cachego</a></td>
<td>Golang Cache component for multiple drivers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/google/cayley">cayley</a></td>
<td>Graph database with support for multiple backends.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/viant/dsc">dsc</a></td>
<td>Datastore connectivity for SQL, NoSQL, structured files.</td>
</tr>
</tbody>
</table>

<h2>Date and Time</h2>

<p><em>Libraries for working with dates and times.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/uniplaces/carbon">carbon</a></td>
<td>Simple Time extension with a lot of util methods, ported from PHP Carbon library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rickb777/date">date</a></td>
<td>Augments Time for working with dates, date ranges, time spans, periods, and time-of-day.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/araddon/dateparse">dateparse</a></td>
<td>Parse date&rsquo;s without knowing format in advance.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hako/durafmt">durafmt</a></td>
<td>Time duration formatting library for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/wlbr/feiertage">feiertage</a></td>
<td>Set of functions to calculate public holidays in Germany, incl. specialization on the states of Germany (Bundesländer). Things like Easter, Pentecost, Thanksgiving&hellip;</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yaa110/go-persian-calendar">go-persian-calendar</a></td>
<td>The implementation of the Persian (Solar Hijri) Calendar in Go (golang).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nathan-osman/go-sunrise">go-sunrise</a></td>
<td>Calculate the sunrise and sunset times for a given location.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/grsmv/goweek">goweek</a></td>
<td>Library for working with week entity in golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/relvacode/iso8601">iso8601</a></td>
<td>Efficiently parse ISO8601 date-times without regex.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/GuilhermeCaruso/Kair">Kair</a></td>
<td>Date and Time - Golang Formatting Library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jinzhu/now">now</a></td>
<td>Now is a time toolkit for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kirillDanshin/nulltime">NullTime</a></td>
<td>Nullable <code>time.Time</code>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/awoodbeck/strftime">strftime</a></td>
<td>C99-compatible strftime formatter.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/SaidinWoT/timespan">timespan</a></td>
<td>For interacting with intervals of time, defined as a start time and a duration.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/leekchan/timeutil">timeutil</a></td>
<td>Useful extensions (Timedelta, Strftime, &hellip;) to the golang&rsquo;s time package.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/osteele/tuesday">tuesday</a></td>
<td>Ruby-compatible Strftime function.</td>
</tr>
</tbody>
</table>

<h2>Distributed Systems</h2>

<p><em>Packages that help with building Distributed Systems.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/svcavallar/celeriac.v1">celeriac</a></td>
<td>Library for adding support for interacting and monitoring Celery workers, tasks and events in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/buraksezer/consistent">consistent</a></td>
<td>Consistent hashing with bounded loads.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/digota/digota">digota</a></td>
<td>grpc ecommerce microservice.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/edwingeng/doublejump">doublejump</a></td>
<td>A revamped Google&rsquo;s jump consistent hash.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dgruber/drmaa">drmaa</a></td>
<td>Job submission library for cluster schedulers based on the DRMAA standard.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/emitter-io/emitter">emitter-io</a></td>
<td>High performance, distributed, secure and low latency publish-subscribe platform built with MQTT, Websockets and love.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/vectaport/flowgraph">flowgraph</a></td>
<td>flow-based programming package.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/chrislusf/gleam">gleam</a></td>
<td>Fast and scalable distributed map/reduce system written in pure Go and Luajit, combining Go&rsquo;s high concurrency with Luajit&rsquo;s high performance, runs standalone or distributed.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/chrislusf/glow">glow</a></td>
<td>Easy-to-Use scalable distributed big data processing, Map-Reduce, DAG execution, all in pure Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/InVisionApp/go-health">go-health</a></td>
<td>Library for enabling asynchronous dependency health checks in your service.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dgryski/go-jump">go-jump</a></td>
<td>Port of Google&rsquo;s &ldquo;Jump&rdquo; Consistent Hash function.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-kit/kit">go-kit</a></td>
<td>Microservice toolkit with support for service discovery, load balancing, pluggable transports, request tracking, etc.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/valyala/gorpc">gorpc</a></td>
<td>Simple, fast and scalable RPC library for high load.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/grpc/grpc-go">grpc-go</a></td>
<td>The Go language implementation of gRPC. HTTP/2 based RPC.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hprose/hprose-golang">hprose</a></td>
<td>Very newbility RPC Library, support 25+ languages now.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jaegertracing/jaeger">jaeger</a></td>
<td>A distributed tracing system.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/osamingo/jsonrpc">jsonrpc</a></td>
<td>The jsonrpc package helps implement of JSON-RPC 2.0.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ybbus/jsonrpc">jsonrpc</a></td>
<td>JSON-RPC 2.0 HTTP client implementation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/devopsfaith/krakend">KrakenD</a></td>
<td>Ultra performant API Gateway framework with middlewares.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/micro/micro">micro</a></td>
<td>Pluggable microservice toolkit and distributed systems platform.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nats-io/gnatsd">NATS</a></td>
<td>Lightweight, high performance messaging system for microservices, IoT, and cloud native systems.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://cirello.io/pglock">pglock</a></td>
<td>PostgreSQL-backed distributed locking implementation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hashicorp/raft">raft</a></td>
<td>Golang implementation of the Raft consensus protocol, by HashiCorp.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/coreos/etcd/tree/master/raft">raft</a></td>
<td>Go implementation of the Raft consensus protocol, by CoreOS.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bsm/redis-lock">redis-lock</a></td>
<td>Simplified distributed locking implementation using Redis.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/uber/ringpop-go">ringpop-go</a></td>
<td>Scalable, fault-tolerant application-layer sharding for Go applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/smallnest/rpcx">rpcx</a></td>
<td>Distributed pluggable RPC service framework like alibaba Dubbo.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ursiform/sleuth">sleuth</a></td>
<td>Library for master-less p2p auto-discovery and RPC between HTTP services (using <a href="https://github.com/zeromq/libzmq">ZeroMQ</a>).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tendermint/tendermint">tendermint</a></td>
<td>High-performance middleware for transforming a state machine written in any programming language into a Byzantine Fault Tolerant replicated state machine using the Tendermint consensus and blockchain protocols.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/anacrolix/torrent">torrent</a></td>
<td>BitTorrent client package.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://godoc.org/github.com/anacrolix/dht">dht</a></td>
<td>BitTorrent Kademlia DHT implementation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Sioro-Neoku/go-peerflix">go-peerflix</a></td>
<td>Video streaming torrent client.</td>
</tr>
</tbody>
</table>

<h2>Email</h2>

<p><em>Libraries and tools that implement email creation and sending.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://blitiri.com.ar/p/chasquid">chasquid</a></td>
<td>SMTP server written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aymerick/douceur">douceur</a></td>
<td>CSS inliner for your HTML emails.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jordan-wright/email">email</a></td>
<td>A robust and flexible email library for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/toorop/go-dkim">go-dkim</a></td>
<td>DKIM library, to sign &amp; verify email.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/emersion/go-imap">go-imap</a></td>
<td>IMAP library for clients and servers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/emersion/go-message">go-message</a></td>
<td>Streaming library for the Internet Message Format and mail messages.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-gomail/gomail/">Gomail</a></td>
<td>Gomail is a very simple and powerful package to send emails.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hectane/hectane">Hectane</a></td>
<td>Lightweight SMTP client providing an HTTP API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/matcornic/hermes">hermes</a></td>
<td>Golang package that generates clean, responsive HTML e-mails.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mailhog/MailHog">MailHog</a></td>
<td>Email and SMTP testing with web and API interface.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sendgrid/sendgrid-go">SendGrid</a></td>
<td>SendGrid&rsquo;s Go library for sending email.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mailhog/smtp">smtp</a></td>
<td>SMTP server protocol state machine.</td>
</tr>
</tbody>
</table>

<h2>Embeddable Scripting Languages</h2>

<p><em>Embedding other languages inside your go code.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/PuerkitoBio/agora">agora</a></td>
<td>Dynamically typed, embeddable programming language in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mattn/anko">anko</a></td>
<td>Scriptable interpreter written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/alexeyco/binder">binder</a></td>
<td>Go to Lua binding library, based on <a href="https://github.com/yuin/gopher-lua">gopher-lua</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/antonmedv/expr">expr</a></td>
<td>an engine that can evaluate expressions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jcla1/gisp">gisp</a></td>
<td>Simple LISP in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/olebedev/go-duktape">go-duktape</a></td>
<td>Duktape JavaScript engine bindings for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Shopify/go-lua">go-lua</a></td>
<td>Port of the Lua 5.2 VM to pure Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/deuill/go-php">go-php</a></td>
<td>PHP bindings for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sbinet/go-python">go-python</a></td>
<td>naive go bindings to the CPython C-API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aarzilli/golua">golua</a></td>
<td>Go bindings for Lua C API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yuin/gopher-lua">gopher-lua</a></td>
<td>Lua 5.1 VM and compiler written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/PaesslerAG/gval">gval</a></td>
<td>A highly customizable expression language written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/db47h/ngaro">ngaro</a></td>
<td>Embeddable Ngaro VM implementation enabling scripting in Retro.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/robertkrimen/otto">otto</a></td>
<td>JavaScript interpreter written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ian-kent/purl">purl</a></td>
<td>Perl 5.18.2 embedded in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/d5/tengo">tengo</a></td>
<td>Bytecode compiled script language for Go.</td>
</tr>
</tbody>
</table>

<h2>Error Handling</h2>

<p><em>Libraries for handling errors.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pkg/errors">errors</a></td>
<td>Package that provides simple error handling primitives.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/joomcode/errorx">errorx</a></td>
<td>A feature rich error package with stack traces, composition of errors and more.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hashicorp/go-multierror">go-multierror</a></td>
<td>Go (golang) package for representing a list of errors as a single error.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/txgruppi/werr">werr</a></td>
<td>Error Wrapper creates an wrapper for the error type in Go which captures the File, Line and Stack of where it was called.</td>
</tr>
</tbody>
</table>

<h2>Files</h2>

<p><em>Libraries for  handling files and file systems.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/spf13/afero">afero</a></td>
<td>FileSystem Abstraction System for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/artonge/go-csv-tag">go-csv-tag</a></td>
<td>Load csv file using tag.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hugocarreira/go-decent-copy">go-decent-copy</a></td>
<td>Copy files for humans.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/artonge/go-gtfs">go-gtfs</a></td>
<td>Load gtfs files in go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rjeczalik/notify">notify</a></td>
<td>File system event notification library with simple API, similar to os/signal.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hhrutter/pdfcpu">pdfcpu</a></td>
<td>PDF processor.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dixonwille/skywalker">skywalker</a></td>
<td>Package to allow one to concurrently go through a filesystem with ease.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/posener/tarfs">tarfs</a></td>
<td>Implementation of the <a href="https://godoc.org/github.com/kr/fs#FileSystem"><code>FileSystem</code> interface</a> for tar files.</td>
</tr>
</tbody>
</table>

<h2>Financial</h2>

<p><em>Packages for accounting and finance.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/leekchan/accounting">accounting</a></td>
<td>money and currency formatting for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shopspring/decimal">decimal</a></td>
<td>Arbitrary-precision fixed-point decimal numbers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/FlashBoys/go-finance">go-finance</a></td>
<td>Comprehensive financial markets data in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/alpeb/go-finance">go-finance</a></td>
<td>Library of financial functions for time value of money (annuities), cash flow, interest rate conversions, bonds and depreciation calculations.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rhymond/go-money">go-money</a></td>
<td>Implementation of Fowler&rsquo;s Money pattern.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aclindsa/ofxgo">ofxgo</a></td>
<td>Query OFX servers and/or parse the responses (with example command-line client).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sdcoffey/techan">techan</a></td>
<td>Technical analysis library with advanced market analysis and trading strategies.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/claygod/transaction">transaction</a></td>
<td>Embedded transactional database of accounts, running in multithreaded mode.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dannyvankooten/vat">vat</a></td>
<td>VAT number validation &amp; EU VAT rates.</td>
</tr>
</tbody>
</table>

<h2>Forms</h2>

<p><em>Libraries for working with forms.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/robfig/bind">bind</a></td>
<td>Bind form data to any Go values.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mholt/binding">binding</a></td>
<td>Binds form and JSON data from net/http Request to struct.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/leebenson/conform">conform</a></td>
<td>Keeps user input in check. Trims, sanitizes &amp; scrubs data based on struct tags.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-playground/form">form</a></td>
<td>Decodes url.Values into Go value(s) and Encodes Go value(s) into url.Values. Dual Array and Full map support.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/monoculum/formam">formam</a></td>
<td>decode form&rsquo;s values into a struct.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/albrow/forms">forms</a></td>
<td>Framework-agnostic library for parsing and validating form/JSON data which supports multipart forms and files.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gorilla/csrf">gorilla/csrf</a></td>
<td>CSRF protection for Go web applications &amp; services.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/justinas/nosurf">nosurf</a></td>
<td>CSRF protection middleware for Go.</td>
</tr>
</tbody>
</table>

<h2>Functional</h2>

<p><em>Packages to support functional programming in Go.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/TeaEntityLab/fpGo">fpGo</a></td>
<td>Monad, Functional Programming features for Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/seborama/fuego">fuego</a></td>
<td>Functional Experiment in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tobyhede/go-underscore">go-underscore</a></td>
<td>Useful collection of helpfully functional Go collection utilities.</td>
</tr>
</tbody>
</table>

<h2>Game Development</h2>

<p><em>Awesome game development libraries.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/azul3d/engine">Azul3D</a></td>
<td>3D game engine written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hajimehoshi/ebiten">Ebiten</a></td>
<td>dead simple 2D game library in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/EngoEngine/engo">engo</a></td>
<td>Engo is an open-source 2D game engine written in Go. It follows the Entity-Component-System paradigm.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/g3n/engine">g3n</a></td>
<td>Go 3D Game Engine.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/vova616/GarageEngine">GarageEngine</a></td>
<td>2d game engine written in Go working on OpenGL.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/runningwild/glop">glop</a></td>
<td>Glop (Game Library Of Power) is a fairly simple cross-platform game library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/beefsack/go-astar">go-astar</a></td>
<td>Go implementation of the A* path finding algorithm.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/GlenKelley/go-collada">go-collada</a></td>
<td>Go package for working with the Collada file format.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/veandco/go-sdl2">go-sdl2</a></td>
<td>Go bindings for the <a href="https://www.libsdl.org/">Simple DirectMedia Layer</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ungerik/go3d">go3d</a></td>
<td>Performance oriented 2D/3D math package for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xtaci/gonet">gonet</a></td>
<td>Game server skeleton implemented with golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xiaonanln/goworld">goworld</a></td>
<td>Scalable game server engine, featuring space-entity framework and hot-swapping.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/name5566/leaf">Leaf</a></td>
<td>Lightweight game server framework.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/lonng/nano">nano</a></td>
<td>Lightweight, facility, high performance golang based game server framework.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/oakmound/oak">Oak</a></td>
<td>Pure Go game engine.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/topfreegames/pitaya">Pitaya</a></td>
<td>Scalable game server framework with clustering support and client libraries for iOS, Android, Unity and others through the C SDK.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/faiface/pixel">Pixel</a></td>
<td>Hand-crafted 2D game library in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gen2brain/raylib-go">raylib-go</a></td>
<td>Go bindings for <a href="http://www.raylib.com/">raylib</a>, a simple and easy-to-use library to learn videogames programming.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/JoelOtter/termloop">termloop</a></td>
<td>Terminal-based game engine for Go, built on top of Termbox.</td>
</tr>
</tbody>
</table>

<h2>Generation and Generics</h2>

<p><em>Tools to enhance the language with features like generics via code generation.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/t0pep0/efaceconv">efaceconv</a></td>
<td>Code generation tool for high performance conversion from interface{} to immutable type without allocations.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/clipperhouse/gen">gen</a></td>
<td>Code generation tool for ‘generics’-like functionality.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/abice/go-enum">go-enum</a></td>
<td>Code generation for enums from code comments.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ahmetalpbalkan/go-linq">go-linq</a></td>
<td>.NET LINQ-like query methods for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/awalterschulze/goderive">goderive</a></td>
<td>Derives functions from input types.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/wzshiming/gotype">gotype</a></td>
<td>Golang source code parsing, usage like reflect package.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hexdigest/gowrap">GoWrap</a></td>
<td>Generate decorators for Go interfaces using simple templates.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rjeczalik/interfaces">interfaces</a></td>
<td>Command line tool for generating interface definitions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dave/jennifer">jennifer</a></td>
<td>Generate arbitrary Go code without templates.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ungerik/pkgreflect">pkgreflect</a></td>
<td>Go preprocessor for package scoped reflection.</td>
</tr>
</tbody>
</table>

<h2>Geographic</h2>

<p><em>Geographic tools and servers</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/melihmucuk/geocache">geocache</a></td>
<td>In-memory cache that is suitable for geolocation based applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hishamkaram/geoserver">geoserver</a></td>
<td>geoserver Is a Go Package For Manipulating a GeoServer Instance via the GeoServer REST API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hishamkaram/gismanager">gismanager</a></td>
<td>Publish Your GIS Data(Vector Data) to PostGIS and Geoserver.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/paulmach/osm">osm</a></td>
<td>Library for reading, writing and working with OpenStreetMap data and APIs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/maguro/pbf">pbf</a></td>
<td>OpenStreetMap PBF golang encoder/decoder.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/golang/geo">S2 geometry</a></td>
<td>S2 geometry library in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tidwall/tile38">Tile38</a></td>
<td>Geolocation DB with spatial index and realtime geofencing.</td>
</tr>
</tbody>
</table>

<h2>Go Compilers</h2>

<p><em>Tools for compiling Go to other languages.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Konstantin8105/c4go">c4go</a></td>
<td>Transpile C code to Go code.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Konstantin8105/f4go">f4go</a></td>
<td>Transpile FORTRAN 77 code to Go code.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gopherjs/gopherjs">gopherjs</a></td>
<td>Compiler from Go to JavaScript.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-llvm/llgo">llgo</a></td>
<td>LLVM-based compiler for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tardisgo/tardisgo">tardisgo</a></td>
<td>Golang to Haxe to CPP/CSharp/Java/JavaScript transpiler.</td>
</tr>
</tbody>
</table>

<h2>Goroutines</h2>

<p><em>Tools for managing and working with Goroutines.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/panjf2000/ants">ants</a></td>
<td>A high-performance goroutine pool for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/borderstech/artifex">artifex</a></td>
<td>Simple in-memory job queue for Golang using worker-based dispatching.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/studiosol/async">async</a></td>
<td>A safe way to execute functions asynchronously, recovering them in case of panic.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/marusama/cyclicbarrier">cyclicbarrier</a></td>
<td>CyclicBarrier for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/workanator/go-floc">go-floc</a></td>
<td>Orchestrate goroutines with ease.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kamildrazkiewicz/go-flow">go-flow</a></td>
<td>Control goroutines execution order.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/subchen/go-trylock">go-trylock</a></td>
<td>TryLock support on read-write lock for Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/themester/GoSlaves">GoSlaves</a></td>
<td>Simple and Asynchronous Goroutine pool library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/benmanns/goworker">goworker</a></td>
<td>goworker is a Go-based background worker.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Sherifabdlnaby/gpool">gpool</a></td>
<td>manages a resizeable pool of context-aware goroutines to bound concurrency.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ivpusic/grpool">grpool</a></td>
<td>Lightweight Goroutine pool.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://cirello.io/oversight">oversight</a></td>
<td>Oversight is a complete implementation of the Erlang supervision trees.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rafaeljesus/parallel-fn">parallel-fn</a></td>
<td>Run functions in parallel.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-playground/pool">pool</a></td>
<td>Limited consumer goroutine or unlimited goroutine pool for easier goroutine handling and cancellation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kamilsk/semaphore">semaphore</a></td>
<td>Semaphore pattern implementation with timeout of lock/unlock operations based on channel and context.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/marusama/semaphore">semaphore</a></td>
<td>Fast resizable semaphore implementation based on CAS (faster than channel-based semaphore implementations).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ssgreg/stl">stl</a></td>
<td>Software transactional locks based on Software Transactional Memory (STM) concurrency control mechanism.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shettyh/threadpool">threadpool</a></td>
<td>Golang threadpool implementation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Jeffail/tunny">tunny</a></td>
<td>Goroutine pool for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/vardius/worker-pool">worker-pool</a></td>
<td>goworker is a Go simple async worker pool.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gammazero/workerpool">workerpool</a></td>
<td>Goroutine pool that limits the concurrency of task execution, not the number of tasks queued.</td>
</tr>
</tbody>
</table>

<h2>GUI</h2>

<p><em>Libraries for building GUI Applications.</em></p>

<p><em>Toolkits</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/murlokswarm/app">app</a></td>
<td>Package to create apps with GO, HTML and CSS. Supports: MacOS, Windows in progress.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fyne-io/fyne">fyne</a></td>
<td>Cross platform native GUIs designed for Go, rendered using EFL. Supports: Linux, macOS, Windows.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/asticode/go-astilectron">go-astilectron</a></td>
<td>Build cross platform GUI apps with GO and HTML/JS/CSS (powered by Electron).</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://mattn.github.io/go-gtk/">go-gtk</a></td>
<td>Go bindings for GTK.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sciter-sdk/go-sciter">go-sciter</a></td>
<td>Go bindings for Sciter: the Embeddable HTML/CSS/script engine for modern desktop UI development. Cross platform.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gotk3/gotk3">gotk3</a></td>
<td>Go bindings for GTK3.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dtylman/gowd">gowd</a></td>
<td>Rapid and simple desktop UI development with GO, HTML, CSS and NW.js. Cross platform.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/therecipe/qt">qt</a></td>
<td>Qt binding for Go (support for Windows / macOS / Linux / Android / iOS / Sailfish OS / Raspberry Pi).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/andlabs/ui">ui</a></td>
<td>Platform-native GUI library for Go. Cross platform.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/lxn/walk">walk</a></td>
<td>Windows application library kit for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zserge/webview">webview</a></td>
<td>Cross-platform webview window with simple two-way JavaScript bindings (Windows / macOS / Linux).</td>
</tr>
</tbody>
</table>
<p><em>Interaction</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/deckarep/gosx-notifier">gosx-notifier</a></td>
<td>OSX Desktop Notifications library for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-vgo/robotgo">robotgo</a></td>
<td>Go Native cross-platform GUI system automation. Control the mouse, keyboard and other.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/getlantern/systray">systray</a></td>
<td>Cross platform Go library to place an icon and menu in the notification area.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shurcooL/trayhost">trayhost</a></td>
<td>Cross-platform Go library to place an icon in the host operating system&rsquo;s taskbar.</td>
</tr>
</tbody>
</table>

<h2>Hardware</h2>

<p><em>Libraries, tools, and tutorials for interacting with hardware.</em></p>

<p>See <a href="https://github.com/rakyll/go-hardware">go-hardware</a> for a comprehensive list.</p>

<h2>Images</h2>

<p><em>Libraries for manipulating images.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/anthonynsimon/bild">bild</a></td>
<td>Collection of image processing algorithms in pure Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/h2non/bimg">bimg</a></td>
<td>Small package for fast and efficient image processing using libvips.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aofei/cameron">cameron</a></td>
<td>An avatar generator for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pravj/geopattern">geopattern</a></td>
<td>Create beautiful generative image patterns from a string.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fogleman/gg">gg</a></td>
<td>2D rendering in pure Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/disintegration/gift">gift</a></td>
<td>Package of image processing filters.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ungerik/go-cairo">go-cairo</a></td>
<td>Go binding for the cairo graphics library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bolknote/go-gd">go-gd</a></td>
<td>Go binding for GD library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/koyachi/go-nude">go-nude</a></td>
<td>Nudity detection with Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/lazywei/go-opencv">go-opencv</a></td>
<td>Go bindings for OpenCV.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jyotiska/go-webcolors">go-webcolors</a></td>
<td>Port of webcolors library from Python to Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hybridgroup/gocv">gocv</a></td>
<td>Go package for computer vision using OpenCV 3.3+.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/corona10/goimagehash">goimagehash</a></td>
<td>Go Perceptual image hashing package.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/o1egl/govatar">govatar</a></td>
<td>Library and CMD tool for generating funny avatars.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/qeesung/image2ascii">image2ascii</a></td>
<td>Convert image to ASCII.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gographics/imagick">imagick</a></td>
<td>Go binding to ImageMagick&rsquo;s MagickWand C API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/h2non/imaginary">imaginary</a></td>
<td>Fast and simple HTTP microservice for image resizing.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/disintegration/imaging">imaging</a></td>
<td>Simple Go image processing package.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hawx/img">img</a></td>
<td>Selection of image manipulation tools.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fogleman/ln">ln</a></td>
<td>3D line art rendering in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/noelyahan/mergi">mergi</a></td>
<td>Tool &amp; Go library for image manipulation (Merge, Crop, Resize, Watermark, Animate).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aldor007/mort">mort</a></td>
<td>Storage and image processing server written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/donatj/mpo">mpo</a></td>
<td>Decoder and conversion tool for MPO 3D Photos.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/thoas/picfit">picfit</a></td>
<td>An image resizing server written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fogleman/pt">pt</a></td>
<td>Path tracing engine written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nfnt/resize">resize</a></td>
<td>Image resizing for Go with common interpolation methods.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bamiaux/rez">rez</a></td>
<td>Image resizing in pure Go and SIMD.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/muesli/smartcrop">smartcrop</a></td>
<td>Finds good crops for arbitrary images and crop sizes.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/auyer/steganography">steganography</a></td>
<td>Pure Go Library for LSB steganography.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/DimitarPetrov/stegify">stegify</a></td>
<td>Go tool for LSB steganography, capable of hiding any file within an image.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ajstarks/svgo">svgo</a></td>
<td>Go Language Library for SVG generation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ftrvxmtrx/tga">tga</a></td>
<td>Package tga is a TARGA image format decoder/encoder.</td>
</tr>
</tbody>
</table>

<h2>IoT (Internet of Things)</h2>

<p><em>Libraries for programming devices of the IoT.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/connectordb/connectordb">connectordb</a></td>
<td>Open-Source Platform for Quantified Self &amp; IoT.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/goiot/devices">devices</a></td>
<td>Suite of libraries for IoT devices, experimental for x/exp/io.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xcodersun/eywa">eywa</a></td>
<td>Project Eywa is essentially a connection manager that keeps track of connected devices.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tibcosoftware/flogo">flogo</a></td>
<td>Project Flogo is an Open Source Framework for IoT Edge Apps &amp; Integration.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/paypal/gatt">gatt</a></td>
<td>Gatt is a Go package for building Bluetooth Low Energy peripherals.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hybridgroup/gobot/">gobot</a></td>
<td>Gobot is a framework for robotics, physical computing, and the Internet of Things.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/amimof/huego">huego</a></td>
<td>An extensive Philips Hue client library for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/vaelen/iot/">iot</a></td>
<td>IoT is a simple framework for implementing a Google IoT Core device.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Mainflux/mainflux">mainflux</a></td>
<td>Industrial IoT Messaging and Device Management Server.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://periph.io/">periph</a></td>
<td>Peripherals I/O to interface with low-level board facilities.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sensorbee/sensorbee">sensorbee</a></td>
<td>Lightweight stream processing engine for IoT.</td>
</tr>
</tbody>
</table>

<h2>Job Scheduler</h2>

<p><em>Libraries for scheduling jobs.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="http://github.com/onatm/clockwerk">clockwerk</a></td>
<td>Go package to schedule periodic jobs using a simple, fluent syntax.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/whiteShtef/clockwork">clockwork</a></td>
<td>Simple and intuitive job scheduling library in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rk/go-cron">go-cron</a></td>
<td>Simple Cron library for go that can execute closures or functions at varying intervals, from once a second to once a year on a specific date and time. Primarily for web applications and long running daemons.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/roylee0704/gron">gron</a></td>
<td>Define time-based tasks using a simple Go API and Gron’s scheduler will run them accordingly.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bamzi/jobrunner">JobRunner</a></td>
<td>Smart and featureful cron job scheduler with job queuing and live monitoring built in.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/albrow/jobs">jobs</a></td>
<td>Persistent and flexible background jobs library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kilgaloon/leprechaun">leprechaun</a></td>
<td>Job scheduler that supports webhooks, crons and classic scheduling.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/carlescere/scheduler">scheduler</a></td>
<td>Cronjobs scheduling made easy.</td>
</tr>
</tbody>
</table>

<h2>JSON</h2>

<p><em>Libraries for working with JSON.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tidwall/gjson">GJSON</a></td>
<td>Get a JSON value with one line of code.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nicklaw5/go-respond">go-respond</a></td>
<td>Go package for handling common HTTP JSON responses.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/elgs/gojq">gojq</a></td>
<td>JSON query in Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ChimeraCoder/gojson">gojson</a></td>
<td>Automatically generate Go (golang) struct definitions from example JSON.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yazgazan/jaydiff">JayDiff</a></td>
<td>JSON diff utility written in Go.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://mholt.github.io/json-to-go/">JSON-to-Go</a></td>
<td>Convert JSON to Go struct.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/AmuzaTkts/jsonapi-errors">jsonapi-errors</a></td>
<td>Go bindings based on the JSON API errors reference.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/miolini/jsonf">jsonf</a></td>
<td>Console tool for highlighted formatting and struct query fetching JSON.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ricardolonga/jsongo">jsongo</a></td>
<td>Fluent API to make it easier to create Json objects.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/RichardKnop/jsonhal">jsonhal</a></td>
<td>Simple Go package to make custom structs marshal into HAL compatible JSON responses.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Qntfy/kazaam">kazaam</a></td>
<td>API for arbitrary transformation of JSON documents.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sanbornm/mp">mp</a></td>
<td>Simple cli email parser. It currently takes stdin and outputs JSON.</td>
</tr>
</tbody>
</table>

<h2>Logging</h2>

<p><em>Libraries for generating and working with log files.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/amoghe/distillog">distillog</a></td>
<td>distilled levelled logging (think of it as stdlib + log levels).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kpango/glg">glg</a></td>
<td>glg is simple and fast leveled logging library for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/lajosbencz/glo">glo</a></td>
<td>PHP Monolog inspired logging facility with identical severity levels.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/golang/glog">glog</a></td>
<td>Leveled execution logs for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/utahta/go-cronowriter">go-cronowriter</a></td>
<td>Simple writer that rotate log files automatically based on current date and time, like cronolog.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/subchen/go-log">go-log</a></td>
<td>Simple and configurable Logging in Go, with level, formatters and writers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/siddontang/go-log">go-log</a></td>
<td>Log lib supports level and multi handlers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ian-kent/go-log">go-log</a></td>
<td>Log4j implementation in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/apsdehal/go-logger">go-logger</a></td>
<td>Simple logger of Go Programs, with level handlers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sadlil/gologger">gologger</a></td>
<td>Simple easy to use log lib for go, logs in Colored Console, Simple Console, File or Elasticsearch.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aphistic/gomol">gomol</a></td>
<td>Multiple-output, structured logging for Go with extensible logging outputs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/One-com/gone/tree/master/log">gone/log</a></td>
<td>Fast, extendable, full-featured, std-lib source compatible log library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ssgreg/journald">journald</a></td>
<td>Go implementation of systemd Journal&rsquo;s native API for logging.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/apex/log">log</a></td>
<td>Structured logging package for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-playground/log">log</a></td>
<td>Simple, configurable and scalable Structured Logging for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/teris-io/log">log</a></td>
<td>Structured log interface for Go cleanly separates logging facade from its implementation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/firstrow/logvoyage">log-voyage</a></td>
<td>Full-featured logging saas written in golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/inconshreveable/log15">log15</a></td>
<td>Simple, powerful logging for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ewwwwwqm/logdump">logdump</a></td>
<td>Package for multi-level logging.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/chzyer/logex">logex</a></td>
<td>Golang log lib, supports tracking and level, wrap by standard log lib.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/azer/logger">logger</a></td>
<td>Minimalistic logging library for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/borderstech/logmatic">logmatic</a></td>
<td>Colorized logger for Golang with dynamic log level configuration.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mbndr/logo">logo</a></td>
<td>Golang logger to different configurable writers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Sirupsen/logrus">logrus</a></td>
<td>Structured logger for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sebest/logrusly">logrusly</a></td>
<td><a href="https://github.com/sirupsen/logrus">logrus</a> plug-in to send errors to a <a href="https://www.loggly.com/">Loggly</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hashicorp/logutils">logutils</a></td>
<td>Utilities for slightly better logging in Go (Golang) extending the standard logger.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mgutz/logxi">logxi</a></td>
<td>12-factor app logger that is fast and makes you happy.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/natefinch/lumberjack">lumberjack</a></td>
<td>Simple rolling logger, implements io.WriteCloser.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jbrodriguez/mlog">mlog</a></td>
<td>Simple logging module for go, with 5 levels, an optional rotating logfile feature and stdout/stderr output.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/francoispqt/onelog">onelog</a></td>
<td>Onelog is a dead simple but very efficient JSON logger. It is the fastest JSON logger out there in all scenario. Also, it is one of the logger with the lowest allocation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-ozzo/ozzo-log">ozzo-log</a></td>
<td>High performance logging supporting log severity, categorization, and filtering. Can send filtered log messages to various targets (e.g. console, network, mail).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cihub/seelog">seelog</a></td>
<td>Logging functionality with flexible dispatching, filtering, and formatting.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/davecgh/go-spew">spew</a></td>
<td>Implements a deep pretty printer for Go data structures to aid in debugging.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/alexcesaro/log">stdlog</a></td>
<td>Stdlog is an object-oriented library providing leveled logging. It is very useful for cron jobs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hpcloud/tail">tail</a></td>
<td>Go package striving to emulate the features of the BSD tail program.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xfxdev/xlog">xlog</a></td>
<td>Plugin architecture and flexible log system for Go, with level ctrl, multiple log target and custom log format.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rs/xlog">xlog</a></td>
<td>Structured logger for <code>net/context</code> aware HTTP handlers with flexible dispatching.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/uber-go/zap">zap</a></td>
<td>Fast, structured, leveled logging in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rs/zerolog">zerolog</a></td>
<td>Zero-allocation JSON logger.</td>
</tr>
</tbody>
</table>

<h2>Machine Learning</h2>

<p><em>Libraries for Machine Learning.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jbrukh/bayesian">bayesian</a></td>
<td>Naive Bayesian Classification for Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ryanbressler/CloudForest">CloudForest</a></td>
<td>Fast, flexible, multi-threaded ensembles of decision trees for machine learning in pure Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/MaxHalford/eaopt">eaopt</a></td>
<td>An evolutionary optimization library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/khezen/evoli">evoli</a></td>
<td>Genetic Algorithm and Particle Swarm Optimization library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Fontinalis/fonet">fonet</a></td>
<td>A Deep Neural Network library written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/e-XpertSolutions/go-cluster">go-cluster</a></td>
<td>Go implementation of the k-modes and k-prototypes clustering algorithms.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/patrikeh/go-deep">go-deep</a></td>
<td>A feature-rich neural network library in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/white-pony/go-fann">go-fann</a></td>
<td>Go bindings for Fast Artificial Neural Networks(FANN) library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/thoj/go-galib">go-galib</a></td>
<td>Genetic Algorithms library written in Go / golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/daviddengcn/go-pr">go-pr</a></td>
<td>Pattern recognition package in Go lang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/goml/gobrain">gobrain</a></td>
<td>Neural Networks written in go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/e-dard/godist">godist</a></td>
<td>Various probability distributions, and associated methods.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tomcraven/goga">goga</a></td>
<td>Genetic algorithm library for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sjwhitworth/golearn">GoLearn</a></td>
<td>General Machine Learning library for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/danieldk/golinear">golinear</a></td>
<td>liblinear bindings for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/surenderthakran/gomind">GoMind</a></td>
<td>A simplistic Neural Network Library in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cdipaolo/goml">goml</a></td>
<td>On-line Machine Learning in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/timkaye11/goRecommend">goRecommend</a></td>
<td>Recommendation Algorithms library written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/chewxy/gorgonia">gorgonia</a></td>
<td>graph-based computational library like Theano for Go that provides primitives for building various machine learning and neural network algorithms.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/asafschers/goscore">goscore</a></td>
<td>Go Scoring API for PMML.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/otiai10/gosseract">gosseract</a></td>
<td>Go package for OCR (Optical Character Recognition), by using Tesseract C++ library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/datastream/libsvm">libsvm</a></td>
<td>libsvm golang version derived work based on LIBSVM 3.14.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/NullHypothesis/mlgo">mlgo</a></td>
<td>This project aims to provide minimalistic machine learning algorithms in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jinyeom/neat">neat</a></td>
<td>Plug-and-play, parallel Go framework for NeuroEvolution of Augmenting Topologies (NEAT).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/schuyler/neural-go">neural-go</a></td>
<td>Multilayer perceptron network implemented in Go, with training via backpropagation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ThePaw/probab">probab</a></td>
<td>Probability distribution functions. Bayesian inference. Written in pure Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/muesli/regommend">regommend</a></td>
<td>Recommendation &amp; collaborative filtering engine.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/eaigner/shield">shield</a></td>
<td>Bayesian text classifier with flexible tokenizers and storage backends for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/galeone/tfgo">tfgo</a></td>
<td>Easy to use Tensorflow bindings: simplifies the usage of the official Tensorflow Go bindings. Define computational graphs in Go, load and execute models trained in Python.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Xamber/Varis">Varis</a></td>
<td>Golang Neural Network.</td>
</tr>
</tbody>
</table>

<h2>Messaging</h2>

<p><em>Libraries that implement messaging systems.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sideshow/apns2">APNs2</a></td>
<td>HTTP/2 Apple Push Notification provider for Go — Send push notifications to iOS, tvOS, Safari and OSX apps.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Jeffail/benthos">Benthos</a></td>
<td>A message streaming bridge between a range of protocols.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/centrifugal/centrifugo">Centrifugo</a></td>
<td>Real-time messaging (Websockets or SockJS) server in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/godbus/dbus">dbus</a></td>
<td>Native Go bindings for D-Bus.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/appleboy/drone-line">drone-line</a></td>
<td>Sending <a href="https://at.line.me/en">Line</a> notifications using a binary, docker or Drone CI.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/olebedev/emitter">emitter</a></td>
<td>Emits events using Go way, with wildcard, predicates, cancellation possibilities and many other good wins.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/agoalofalife/event">event</a></td>
<td>Implementation of the pattern observer.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/asaskevich/EventBus">EventBus</a></td>
<td>The lightweight event bus with async compatibility.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/osamingo/gaurun-client">gaurun-client</a></td>
<td>Gaurun Client written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/desertbit/glue">Glue</a></td>
<td>Robust Go and Javascript Socket Library (Alternative to Socket.io).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/TheCreeper/go-notify">go-notify</a></td>
<td>Native implementation of the freedesktop notification spec.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nsqio/go-nsq">go-nsq</a></td>
<td>the official Go package for NSQ.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/googollee/go-socket.io">go-socket.io</a></td>
<td>socket.io library for golang, a realtime application framework.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/maxatome/go-vitotrol">go-vitotrol</a></td>
<td>Client library to Viessmann Vitotrol web service.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/trivago/gollum">Gollum</a></td>
<td>A n:m multiplexer that gathers messages from different sources and broadcasts them to a set of destinations.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jcuga/golongpoll">golongpoll</a></td>
<td>HTTP longpoll server library that makes web pub-sub simple.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ian-kent/goose">goose</a></td>
<td>Server Sent Events in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Terry-Mao/gopush-cluster">gopush-cluster</a></td>
<td>gopush-cluster is a go push server cluster.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/appleboy/gorush">gorush</a></td>
<td>Push notification server using <a href="https://github.com/sideshow/apns2">APNs2</a> and google <a href="https://github.com/google/go-gcm">GCM</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/smancke/guble">guble</a></td>
<td>Messaging server using push notifications (Google Firebase Cloud Messaging, Apple Push Notification services, SMS) as well as websockets, a REST API, featuring distributed operation and message-persistence.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/leandro-lugaresi/hub">hub</a></td>
<td>A Message/Event Hub for Go applications, using publish/subscribe pattern with support for alias like rabbitMQ exchanges.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/RichardKnop/machinery">machinery</a></td>
<td>Asynchronous task queue/job queue based on distributed message passing.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-mangos/mangos">mangos</a></td>
<td>Pure go implementation of the Nanomsg (&ldquo;Scalable Protocols&rdquo;) with transport interoperability.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/olahol/melody">melody</a></td>
<td>Minimalist framework for dealing with websocket sessions, includes broadcasting and automatic ping/pong handling.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dunglas/mercure">Mercure</a></td>
<td>Server and library to dispatch server-sent updates using the Mercure protocol (built on top of Server-Sent Events).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/vardius/message-bus">messagebus</a></td>
<td>messagebus is a Go simple async message bus, perfect for using as event bus when doing event sourcing, CQRS, DDD.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nats-io/nats">NATS Go Client</a></td>
<td>Lightweight and high performance publish-subscribe and distributed queueing messaging system - this is the Go library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rafaeljesus/nsq-event-bus">nsq-event-bus</a></td>
<td>A tiny wrapper around NSQ topic and channel.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dailymotion/oplog">oplog</a></td>
<td>Generic oplog/replication system for REST APIs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tuxychandru/pubsub">pubsub</a></td>
<td>Simple pubsub package for go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rafaeljesus/rabbus">rabbus</a></td>
<td>A tiny wrapper over amqp exchanges and queues.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jandelgado/rabtap">rabtap</a></td>
<td>RabbitMQ swiss army knife cli app.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sybrexsys/RapidMQ">RapidMQ</a></td>
<td>RapidMQ is a lightweight and reliable library for managing of the local messages queue.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Shopify/sarama">sarama</a></td>
<td>Go library for Apache Kafka.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/uniqush/uniqush-push">Uniqush-Push</a></td>
<td>Redis backed unified push service for server-side notifications to mobile devices.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pebbe/zmq4">zmq4</a></td>
<td>Go interface to ZeroMQ version 4. Also available for <a href="https://github.com/pebbe/zmq3">version 3</a> and <a href="https://github.com/pebbe/zmq2">version 2</a>.</td>
</tr>
</tbody>
</table>

<h2>Microsoft Office</h2>

<h3>Microsoft Excel</h3>

<p><em>Libraries for working with Microsoft Excel.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/360EntSecGroup-Skylar/excelize">excelize</a></td>
<td>Golang library for reading and writing Microsoft Excel™ (XLSX) files.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/szyhf/go-excel">go-excel</a></td>
<td>A simple and light reader to read a relate-db-like excel as a table.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fterrag/goxlsxwriter">goxlsxwriter</a></td>
<td>Golang bindings for libxlsxwriter for writing XLSX (Microsoft Excel) files.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tealeg/xlsx">xlsx</a></td>
<td>Library to simplify reading the XML format used by recent version of Microsoft Excel in Go programs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/plandem/xlsx">xlsx</a></td>
<td>Fast and safe way to read/update your existing Microsoft Excel files in Go programs.</td>
</tr>
</tbody>
</table>

<h2>Miscellaneous</h2>

<h3>Dependency Injection</h3>

<p><em>Libraries for working with dependency injection.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/magic003/alice">alice</a></td>
<td>Additive dependency injection container for Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Fs02/wire">wire</a></td>
<td>Strict Runtime Dependency Injection for Golang.</td>
</tr>
</tbody>
</table>

<h3>Strings</h3>

<p><em>Libraries for working with strings.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ozgio/strutil">strutil</a></td>
<td>String utilities.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/huandu/xstrings">xstrings</a></td>
<td>Collection of useful string functions ported from other languages.</td>
</tr>
</tbody>
</table>
<p><em>These libraries were placed here because none of the other categories seemed to fit.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mudler/anagent">anagent</a></td>
<td>Minimalistic, pluggable Golang evloop/timer handler with dependency-injection.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/antchfx/antch">antch</a></td>
<td>A fast, powerful and extensible web crawling &amp; scraping framework.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mholt/archiver">archiver</a></td>
<td>Library and command for making and extracting .zip and .tar.gz archives.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/artyom/autoflags">autoflags</a></td>
<td>Go package to automatically define command line flags from struct fields.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kirillDanshin/avgRating">avgRating</a></td>
<td>Calculate average score and rating based on Wilson Score Equation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dimiro1/banner">banner</a></td>
<td>Add beautiful banners into your Go applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mojocn/base64Captcha">base64Captcha</a></td>
<td>Base64captch supports digit, number, alphabet, arithmetic, audio and digit-alphabet captcha.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/distatus/battery">battery</a></td>
<td>Cross-platform, normalized battery information library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/icza/bitio">bitio</a></td>
<td>Highly optimized bit-level Reader and Writer for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/digitalcrab/browscap_go">browscap_go</a></td>
<td>GoLang Library for <a href="http://browscap.org/">Browser Capabilities Project</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/steambap/captcha">captcha</a></td>
<td>Package captcha provides an easy to use, unopinionated API for captcha generation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cstockton/go-conv">conv</a></td>
<td>Package conv provides fast and intuitive conversions across Go types.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/miolini/datacounter">datacounter</a></td>
<td>Go counters for readers/writer/http.ResponseWriter.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-ffmt/ffmt">ffmt</a></td>
<td>Beautify data display for Humans.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gabrie30/ghorg">ghorg</a></td>
<td>Clone all repos from a GitHub org into a single directory.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jolestar/go-commons-pool">go-commons-pool</a></td>
<td>Generic object pool for Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-openapi">go-openapi</a></td>
<td>Collection of packages to parse and utilize open-api schemas.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/eapache/go-resiliency">go-resiliency</a></td>
<td>Resiliency patterns for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gen2brain/go-unarr">go-unarr</a></td>
<td>Decompression library for RAR, TAR, ZIP and 7z archives.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/brianvoe/gofakeit">gofakeit</a></td>
<td>Random data generator written in go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/antham/gommit">gommit</a></td>
<td>Analyze git commit messages to ensure they follow defined patterns.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shirou/gopsutil">gopsutil</a></td>
<td>Cross-platform library for retrieving process and system utilization(CPU, Memory, Disks, etc).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/osamingo/gosh">gosh</a></td>
<td>Provide Go Statistics Handler, Struct, Measure Method.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/haxpax/gosms">gosms</a></td>
<td>Your own local SMS gateway in Go that can be used to send SMS.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pariz/gountries">gountries</a></td>
<td>Package that exposes country and subdivision data.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dimiro1/health">health</a></td>
<td>Easy to use, extensible health check library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/etherlabsio/healthcheck">healthcheck</a></td>
<td>An opinionated and concurrent health-check HTTP handler for RESTful services.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Wing924/hostutils">hostutils</a></td>
<td>A golang library for packing and unpacking FQDNs list.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/osamingo/indigo">indigo</a></td>
<td>Distributed unique ID generator of using Sonyflake and encoded by Base58.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hyperboloide/lk">lk</a></td>
<td>A simple licensing library for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/llir/llvm">llvm</a></td>
<td>Library for interacting with LLVM IR in pure Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/alwindoss/morse">morse</a></td>
<td>Library to convert to and from morse code.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hyperboloide/pdfgen">pdfgen</a></td>
<td>HTTP service to generate PDF from Json requests.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mavihq/persian">persian</a></td>
<td>Some utilities for Persian language in go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aofei/sandid">sandid</a></td>
<td>Every grain of sand on earth has its own ID.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Wing924/shellwords">shellwords</a></td>
<td>A Golang library to manipulate strings according to the word parsing rules of the UNIX Bourne shell.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/teris-io/shortid">shortid</a></td>
<td>Distributed generation of super short, unique, non-sequential, URL friendly IDs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-playground/stats">stats</a></td>
<td>Monitors Go MemStats + System stats such as Memory, Swap and CPU and sends via UDP anywhere you want for logging etc&hellip;</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hackebrot/turtle">turtle</a></td>
<td>Emojis for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pantrif/url-shortener">url-shortener</a></td>
<td>A modern, powerful, and robust URL shortener microservice with mysql support.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/azr/generators/tree/master/varhandler">VarHandler</a></td>
<td>Generate boilerplate http input and output handling.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rkoesters/xdg">xdg</a></td>
<td>FreeDesktop.org (xdg) Specs implemented in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-xkg/xkg">xkg</a></td>
<td>X Keyboard Grabber.</td>
</tr>
</tbody>
</table>

<h2>Natural Language Processing</h2>

<p><em>Libraries for working with human languages.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rylans/getlang">getlang</a></td>
<td>Fast natural language detection package.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ThePaw/go-eco">go-eco</a></td>
<td>Similarity, dissimilarity and distance matrices; diversity, equitability and inequality measures; species richness estimators; coenocline models.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nicksnyder/go-i18n/">go-i18n</a></td>
<td>Package and an accompanying tool to work with localized text.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dveselov/mystem">go-mystem</a></td>
<td>CGo bindings to Yandex.Mystem - russian morphology analyzer.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nuance/go-nlp">go-nlp</a></td>
<td>Utilities for working with discrete probability distributions and other tools useful for doing NLP work.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mozillazg/go-pinyin">go-pinyin</a></td>
<td>CN Hanzi to Hanyu Pinyin converter.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/agonopol/go-stem">go-stem</a></td>
<td>Implementation of the porter stemming algorithm.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mozillazg/go-unidecode">go-unidecode</a></td>
<td>ASCII transliterations of Unicode text.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/danieldk/go2vec">go2vec</a></td>
<td>Reader and utility functions for word2vec embeddings.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yanyiwu/gojieba">gojieba</a></td>
<td>This is a Go implementation of <a href="https://github.com/fxsjy/jieba">jieba</a> which a Chinese word splitting algorithm.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rjohnsondev/golibstemmer">golibstemmer</a></td>
<td>Go bindings for the snowball libstemmer library including porter 2.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xujiajun/gotokenizer">gotokenizer</a></td>
<td>A tokenizer based on the dictionary and Bigram language models for Golang. (Now only support chinese segmentation)</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fiam/gounidecode">gounidecode</a></td>
<td>Unicode transliterator (also known as unidecode) for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-ego/gse">gse</a></td>
<td>Go efficient text segmentation; support english, chinese, japanese and other.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/goodsign/icu">icu</a></td>
<td>Cgo binding for icu4c C library detection and conversion functions. Guaranteed compatibility with version 50.1.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ikawaha/kagome">kagome</a></td>
<td>JP morphological analyzer written in pure Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/goodsign/libtextcat">libtextcat</a></td>
<td>Cgo binding for libtextcat C library. Guaranteed compatibility with version 2.2.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/awsong/MMSEGO">MMSEGO</a></td>
<td>This is a GO implementation of <a href="http://technology.chtsai.org/mmseg/">MMSEG</a> which a Chinese word splitting algorithm.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Shixzie/nlp">nlp</a></td>
<td>Extract values from strings and fill your structs with nlp.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/james-bowman/nlp">nlp</a></td>
<td>Go Natural Language Processing library supporting LSA (Latent Semantic Analysis).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rookii/paicehusk">paicehusk</a></td>
<td>Golang implementation of the Paice/Husk Stemming Algorithm.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/striker2000/petrovich">petrovich</a></td>
<td>Petrovich is the library which inflects Russian names to given grammatical case.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/a2800276/porter">porter</a></td>
<td>This is a fairly straightforward port of Martin Porter&rsquo;s C implementation of the Porter stemming algorithm.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zhenjl/porter2">porter2</a></td>
<td>Really fast Porter 2 stemmer.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jdkato/prose">prose</a></td>
<td>Library for text processing that supports tokenization, part-of-speech tagging, named-entity extraction, and more.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Obaied/RAKE.go">RAKE.go</a></td>
<td>Go port of the Rapid Automatic Keyword Extraction Algorithm (RAKE).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/blevesearch/segment">segment</a></td>
<td>Go library for performing Unicode Text Segmentation as described in <a href="http://www.unicode.org/reports/tr29/">Unicode Standard Annex #29</a></td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/neurosnap/sentences">sentences</a></td>
<td>Sentence tokenizer:  converts text into a list of sentences.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/osamingo/shamoji">shamoji</a></td>
<td>The shamoji is word filtering package written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/goodsign/snowball">snowball</a></td>
<td>Snowball stemmer port (cgo wrapper) for Go. Provides word stem extraction functionality <a href="http://snowball.tartarus.org/">Snowball native</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dchest/stemmer">stemmer</a></td>
<td>Stemmer packages for Go programming language. Includes English and German stemmers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pebbe/textcat">textcat</a></td>
<td>Go package for n-gram based text categorization, with support for utf-8 and raw text.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/abadojack/whatlanggo">whatlanggo</a></td>
<td>Natural language detection package for Go. Supports 84 languages and 24 scripts (writing systems e.g. Latin, Cyrillic, etc).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/olebedev/when">when</a></td>
<td>Natural EN and RU language date/time parser with pluggable rules.</td>
</tr>
</tbody>
</table>

<h2>Networking</h2>

<p><em>Libraries for working with various layers of the network.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mdlayher/arp">arp</a></td>
<td>Package arp implements the ARP protocol, as described in RFC 826.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/stabbycutyou/buffstreams">buffstreams</a></td>
<td>Streaming protocolbuffer data over TCP made easy.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zubairhamed/canopus">canopus</a></td>
<td>CoAP Client/Server implementation (RFC 7252).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yl2chen/cidranger">cidranger</a></td>
<td>Fast IP to CIDR lookup for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mdlayher/dhcp6">dhcp6</a></td>
<td>Package dhcp6 implements a DHCPv6 server, as described in RFC 3315.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/miekg/dns">dns</a></td>
<td>Go library for working with DNS.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/songgao/ether">ether</a></td>
<td>Cross-platform Go package for sending and receiving ethernet frames.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mdlayher/ethernet">ethernet</a></td>
<td>Package ethernet implements marshaling and unmarshaling of IEEE 802.3 Ethernet II frames and IEEE 802.1Q VLAN tags.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/valyala/fasthttp">fasthttp</a></td>
<td>Package fasthttp is a fast HTTP implementation for Go, up to 10 times faster than net/http.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fortio/fortio">fortio</a></td>
<td>Load testing library and command line tool, advanced echo server and web UI. Allows to specify a set query-per-second load and record latency histograms and other useful stats and graph them. Tcp, Http, gRPC.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jlaffaye/ftp">ftp</a></td>
<td>Package ftp implements a FTP client as described in <a href="http://tools.ietf.org/html/rfc959">RFC 959</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/DrmagicE/gmqtt">gmqtt</a></td>
<td>Gmqtt is a flexible, high-performance MQTT broker library that fully implements the MQTT protocol V3.1.1.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/google/gnxi">gNxI</a></td>
<td>A collection of tools for Network Management that use the gNMI and gNOI protocols.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hashicorp/go-getter">go-getter</a></td>
<td>Go library for downloading files or directories from various sources using a URL.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ccding/go-stun">go-stun</a></td>
<td>Go implementation of the STUN client (RFC 3489 and RFC 5389).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/osrg/gobgp">gobgp</a></td>
<td>BGP implemented in the Go Programming Language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sunwxg/golibwireshark">golibwireshark</a></td>
<td>Package golibwireshark use libwireshark library to decode pcap file and analyse dissection data.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/google/gopacket">gopacket</a></td>
<td>Go library for packet processing with libpcap bindings.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/akrennmair/gopcap">gopcap</a></td>
<td>Go wrapper for libpcap.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sunwxg/goshark">goshark</a></td>
<td>Package goshark use tshark to decode IP packet and create data struct to analyse packet.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/soniah/gosnmp">gosnmp</a></td>
<td>Native Go library for performing SNMP actions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gansidui/gotcp">gotcp</a></td>
<td>Go package for quickly writing tcp applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cavaliercoder/grab">grab</a></td>
<td>Go package for managing file downloads.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/koofr/graval">graval</a></td>
<td>Experimental FTP server framework.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gchaincl/httplab">HTTPLab</a></td>
<td>HTTPLabs let you inspect HTTP requests and forge responses.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/udhos/jazigo">jazigo</a></td>
<td>Jazigo is a tool written in Go for retrieving configuration for multiple network devices.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xtaci/kcp-go">kcp-go</a></td>
<td>KCP - Fast and Reliable ARQ Protocol.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xtaci/kcptun">kcptun</a></td>
<td>Extremely simple &amp; fast udp tunnel based on KCP protocol.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fanux/lhttp">lhttp</a></td>
<td>Powerful websocket framework, build your IM server more easily.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ian-kent/linkio">linkio</a></td>
<td>Network link speed simulation for Reader/Writer interfaces.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kirillDanshin/llb">llb</a></td>
<td>It&rsquo;s a very simple but quick backend for proxy servers. Can be useful for fast redirection to predefined domain with zero memory allocation and fast response.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hashicorp/mdns">mdns</a></td>
<td>Simple mDNS (Multicast DNS) client/server library in Golang.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://eclipse.org/paho/clients/golang/">mqttPaho</a></td>
<td>The Paho Go Client provides an MQTT client library for connection to MQTT brokers via TCP, TLS or WebSockets.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/intel-go/nff-go">NFF-Go</a></td>
<td>Framework for rapid development of performant network functions for cloud and bare-metal (former YANFF).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aerogo/packet">packet</a></td>
<td>Send packets over TCP and UDP. It can buffer messages and hot-swap connections if needed.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/schollz/peerdiscovery">peerdiscovery</a></td>
<td>Pure Go library for cross-platform local peer discovery using UDP multicast.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aybabtme/portproxy">portproxy</a></td>
<td>Simple TCP proxy which adds CORS support to API&rsquo;s which don&rsquo;t support it.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/polera/publicip">publicip</a></td>
<td>Package publicip returns your public facing IPv4 address (internet egress).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/lucas-clemente/quic-go">quic-go</a></td>
<td>An implementation of the QUIC protocol in pure Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mdlayher/raw">raw</a></td>
<td>Package raw enables reading and writing data at the device driver level for a network interface.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pkg/sftp">sftp</a></td>
<td>Package sftp implements the SSH File Transfer Protocol as described in <a href="https://filezilla-project.org/specs/draft-ietf-secsh-filexfer-02.txt">https://filezilla-project.org/specs/draft-ietf-secsh-filexfer-02.txt</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gliderlabs/ssh">ssh</a></td>
<td>Higher-level API for building SSH servers (wraps crypto/ssh).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/eduardonunesp/sslb">sslb</a></td>
<td>It&rsquo;s a Super Simples Load Balancer, just a little project to achieve some kind of performance.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-rtc/stun">stun</a></td>
<td>Go implementation of RFC 5389 STUN protocol.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/firstrow/tcp_server">tcp_server</a></td>
<td>Go library for building tcp servers faster.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/two/tspool">tspool</a></td>
<td>A TCP Library use worker pool to improve performance and protect your server.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/anacrolix/utp">utp</a></td>
<td>Go uTP micro transport protocol implementation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/songgao/water">water</a></td>
<td>Simple TUN/TAP library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pions/webrtc">webrtc</a></td>
<td>A pure Go implementation of the WebRTC API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/masterzen/winrm">winrm</a></td>
<td>Go WinRM client to remotely execute commands on Windows machines.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xfxdev/xtcp">xtcp</a></td>
<td>TCP Server Framework with simultaneous full duplex communication,graceful shutdown,custom protocol.</td>
</tr>
</tbody>
</table>

<h3>HTTP Clients</h3>

<p><em>Libraries for making HTTP requests.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/h2non/gentleman">gentleman</a></td>
<td>Full-featured plugin-driven HTTP client library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/smallnest/goreq">goreq</a></td>
<td>Enhanced simplified HTTP client based on gorequest.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/levigross/grequests">grequests</a></td>
<td>A Go &ldquo;clone&rdquo; of the great and famous Requests library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gojektech/heimdall">heimdall</a></td>
<td>An enchanced http client with retry and hystrix capabilities.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sethgrid/pester">pester</a></td>
<td>Go HTTP client calls with retries, backoff, and concurrency.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ddo/rq">rq</a></td>
<td>A nicer interface for golang stdlib HTTP client.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dghubble/sling">sling</a></td>
<td>Sling is a Go HTTP client library for creating and sending API requests.</td>
</tr>
</tbody>
</table>

<h2>OpenGL</h2>

<p><em>Libraries for using OpenGL in Go.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-gl/gl">gl</a></td>
<td>Go bindings for OpenGL (generated via glow).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-gl/glfw">glfw</a></td>
<td>Go bindings for GLFW 3.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/goxjs/gl">goxjs/gl</a></td>
<td>Go cross-platform OpenGL bindings (OS X, Linux, Windows, browsers, iOS, Android).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/goxjs/glfw">goxjs/glfw</a></td>
<td>Go cross-platform glfw library for creating an OpenGL context and receiving events.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-gl/mathgl">mathgl</a></td>
<td>Pure Go math package specialized for 3D math, with inspiration from GLM.</td>
</tr>
</tbody>
</table>

<h2>ORM</h2>

<p><em>Libraries that implement Object-Relational Mapping or datamapping techniques.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/astaxie/beego/tree/master/orm">beego orm</a></td>
<td>Powerful orm framework for go. Support: pq/mysql/sqlite3.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-pg/pg">go-pg</a></td>
<td>PostgreSQL ORM with focus on PostgreSQL specific features and performance.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jirfag/go-queryset">go-queryset</a></td>
<td>100% type-safe ORM with code generation and MySQL, PostgreSQL, Sqlite3, SQL Server support based on GORM.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/huandu/go-sqlbuilder">go-sqlbuilder</a></td>
<td>A flexible and powerful SQL string builder library plus a zero-config ORM.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gosuri/go-store">go-store</a></td>
<td>Simple and fast Redis backed key-value store library for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cosiner/gomodel">gomodel</a></td>
<td>Lightweight, fast, orm-like library helps interactive with database.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jinzhu/gorm">GORM</a></td>
<td>The fantastic ORM library for Golang, aims to be developer friendly.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-gorp/gorp">gorp</a></td>
<td>Go Relational Persistence, ORM-ish library for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Fs02/grimoire">grimoire</a></td>
<td>Grimoire is a database access layer and validation for golang. (Support: MySQL, PostgreSQL and SQLite3).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/abrahambotros/lore">lore</a></td>
<td>Simple and lightweight pseudo-ORM/pseudo-struct-mapping environment for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dadleyy/marlow">Marlow</a></td>
<td>Generated ORM from project structs for compile time safety assurances.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gobuffalo/pop">pop/soda</a></td>
<td>Database migration, creation, ORM, etc&hellip; for MySQL, PostgreSQL, and SQLite.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/coocood/qbs">QBS</a></td>
<td>Stands for Query By Struct. A Go ORM.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-reform/reform">reform</a></td>
<td>Better ORM for Go, based on non-empty interfaces and code generation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/volatiletech/sqlboiler">SQLBoiler</a></td>
<td>ORM generator. Generate a featureful and blazing-fast ORM tailored to your database schema.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/upper/db">upper.io/db</a></td>
<td>Single interface for interacting with different data sources through the use of adapters that wrap mature database drivers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-xorm/xorm">Xorm</a></td>
<td>Simple and powerful ORM for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/albrow/zoom">Zoom</a></td>
<td>Blazing-fast datastore and querying engine built on Redis.</td>
</tr>
</tbody>
</table>

<h2>Package Management</h2>

<p><em>Official tooling for package management</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/golang/dep">dep</a></td>
<td>Go dependency tool.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://go.googlesource.com/vgo/">vgo</a></td>
<td>Versioned Go.</td>
</tr>
</tbody>
</table>
<p><em>Unofficial libraries for package and dependency management.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/LyricalSecurity/gigo">gigo</a></td>
<td>PIP-like dependency tool for golang, with support for private repositories and hashes.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Masterminds/glide">glide</a></td>
<td>Manage your golang vendor and vendored packages with ease. Inspired by tools like Maven, Bundler, and Pip.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tools/godep">godep</a></td>
<td>dependency tool for go, godep helps build packages reproducibly by fixing their dependencies.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mattn/gom">gom</a></td>
<td>Go Manager - bundle for go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nitrous-io/goop">goop</a></td>
<td>Simple dependency manager for Go (golang), inspired by Bundler.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/lunny/gop">gop</a></td>
<td>Build and manage your Go applications out of GOPATH.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gpmgo/gopm">gopm</a></td>
<td>Go Package Manager.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kardianos/govendor">govendor</a></td>
<td>Go Package Manager. Go vendor tool that works with the standard vendor file.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pote/gpm">gpm</a></td>
<td>Barebones dependency manager for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/VividCortex/johnny-deps">johnny-deps</a></td>
<td>Minimal dependency version using Git.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/raydac/mvn-golang">mvn-golang</a></td>
<td>plugin that provides way for auto-loading of Golang SDK, dependency management and start build environment in Maven project infrastructure.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jingweno/nut">nut</a></td>
<td>Vendor Go dependencies.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/DamnWidget/VenGO">VenGO</a></td>
<td>create and manage exportable isolated go virtual environments.</td>
</tr>
</tbody>
</table>

<h2>Query Language</h2>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/thedevsaddam/gojsonq">gojsonq</a></td>
<td>A simple Go package to Query over JSON Data.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tmc/graphql">graphql</a></td>
<td>graphql parser + utilities.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/neelance/graphql-go">graphql</a></td>
<td>GraphQL server with a focus on ease of use.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/graphql-go/graphql">graphql-go</a></td>
<td>Implementation of GraphQL for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/elgs/jsonql">jsonql</a></td>
<td>JSON query expression library in Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bhmj/jsonslice">jsonslice</a></td>
<td>Jsonpath queries with advanced filters.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/a8m/rql">rql</a></td>
<td>Resource Query Language for REST API.</td>
</tr>
</tbody>
</table>

<h2>Resource Embedding</h2>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mjibson/esc">esc</a></td>
<td>Embeds files into Go programs and provides http.FileSystem interfaces to them.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/UnnoTed/fileb0x">fileb0x</a></td>
<td>Simple tool to embed files in go with focus on &ldquo;customization&rdquo; and ease to use.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pyros2097/go-embed">go-embed</a></td>
<td>Generates go code to embed resource files into your library or executable.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/omeid/go-resources">go-resources</a></td>
<td>Unfancy resources embedding with Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/GeertJohan/go.rice">go.rice</a></td>
<td>go.rice is a Go package that makes working with resources such as html,js,css,images and templates very easy.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gobuffalo/packr">packr</a></td>
<td>The simple and easy way to embed static files into Go binaries.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-playground/statics">statics</a></td>
<td>Embeds static resources into go files for single binary compilation + works with http.FileSystem + symlinks.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rakyll/statik">statik</a></td>
<td>Embeds static files into a Go executable.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/wlbr/templify">templify</a></td>
<td>Embed external template files into Go code to create single file binaries.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shurcooL/vfsgen">vfsgen</a></td>
<td>Generates a vfsdata.go file that statically implements the given virtual filesystem.</td>
</tr>
</tbody>
</table>

<h2>Science and Data Analysis</h2>

<p><em>Libraries for scientific computing and data analyzing.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/vdobler/chart">chart</a></td>
<td>Simple Chart Plotting library for Go. Supports many graphs types.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rocketlaunchr/dataframe-go">dataframe-go</a></td>
<td>Dataframes for Go for machine-learning and statistics (similar to pandas).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/soniah/evaler">evaler</a></td>
<td>Simple floating point arithmetic expression evaluator.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/VividCortex/ewma">ewma</a></td>
<td>Exponentially-weighted moving averages.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/skelterjohn/geom">geom</a></td>
<td>2D geometry for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mjibson/go-dsp">go-dsp</a></td>
<td>Digital Signal Processing for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ematvey/go-fn">go-fn</a></td>
<td>Mathematical functions written in Go language, that are not covered by math pkg.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ThePaw/go-gt">go-gt</a></td>
<td>Graph theory algorithms written in &ldquo;Go&rdquo; language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/varver/gocomplex">gocomplex</a></td>
<td>Complex number library for the Go programming language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kzahedi/goent">goent</a></td>
<td>GO Implementation of Entropy Measures.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/VividCortex/gohistogram">gohistogram</a></td>
<td>Approximate histograms for data streams.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gonum/gonum">gonum</a></td>
<td>Gonum is a set of numeric libraries for the Go programming language. It contains libraries for matrices, statistics, optimization, and more.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gonum/plot">gonum/plot</a></td>
<td>gonum/plot provides an API for building and drawing plots in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gyuho/goraph">goraph</a></td>
<td>Pure Go graph theory library(data structure, algorith visualization).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cpmech/gosl">gosl</a></td>
<td>Go scientific library for linear algebra, FFT, geometry, NURBS, numerical methods, probabilities, optimisation, differential equations, and more.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/OGFris/GoStats">GoStats</a></td>
<td>GoStats is an Open Source GoLang library for math statistics mostly used in Machine Learning domains, it covers most of the Statistical measures functions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yourbasic/graph">graph</a></td>
<td>Library of basic graph algorithms.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ChristopherRabotin/ode">ode</a></td>
<td>Ordinary differential equation (ODE) solver which supports extended states and channel-based iteration stop conditions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/paulmach/orb">orb</a></td>
<td>2D geometry types with clipping, GeoJSON and Mapbox Vector Tile support.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/alixaxel/pagerank">pagerank</a></td>
<td>Weighted PageRank algorithm implemented in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sgreben/piecewiselinear">piecewiselinear</a></td>
<td>Tiny linear interpolation library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/claygod/PiHex">PiHex</a></td>
<td>Implementation of the &ldquo;Bailey-Borwein-Plouffe&rdquo; algorithm for the hexadecimal number Pi.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/khezen/rootfinding">rootfinding</a></td>
<td>root-finding algorithms library for finding roots of quadratic functions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/james-bowman/sparse">sparse</a></td>
<td>Go Sparse matrix formats for linear algebra supporting scientific and machine learning applications, compatible with gonum matrix libraries.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/montanaflynn/stats">stats</a></td>
<td>Statistics package with common functions missing from the Golang standard library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nytlabs/streamtools">streamtools</a></td>
<td>general purpose, graphical tool for dealing with streams of data.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/DavidBelicza/TextRank">TextRank</a></td>
<td>TextRank implementation in Golang with extendable features (summarization, weighting, phrase extraction) and multithreading (goroutine) support.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tchayen/triangolatte">triangolatte</a></td>
<td>2D triangulation library. Allows translating lines and polygons (both based on points) to the language of GPUs.</td>
</tr>
</tbody>
</table>

<h2>Security</h2>

<p><em>Libraries that are used to help make your application more secure.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hlandau/acme">acmetool</a></td>
<td>ACME (Let&rsquo;s Encrypt) client tool with automatic renewal.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cossacklabs/acra">acra</a></td>
<td>Network encryption proxy to protect database-based applications from data leaks: strong selective encryption, SQL injections prevention, intrusion detection system.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/raja/argon2pw">argon2pw</a></td>
<td>Argon2 password hash generation with constant-time password comparison.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://godoc.org/golang.org/x/crypto/acme/autocert">autocert</a></td>
<td>Auto provision Let&rsquo;s Encrypt certificates and start a TLS server.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jaredfolkins/badactor">BadActor</a></td>
<td>In-memory, application-driven jailer built in the spirit of fail2ban.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Ullaakut/cameradar">Cameradar</a></td>
<td>Tool and library to remotely hack RTSP streams from surveillance cameras.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hillu/go-yara">go-yara</a></td>
<td>Go Bindings for <a href="https://github.com/plusvic/yara">YARA</a>, the &ldquo;pattern matching swiss knife for malware researchers (and everyone else)&rdquo;.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dwin/goArgonPass">goArgonPass</a></td>
<td>Argon2 password hash and verification designed to be compatible with existing Python and PHP implementations.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dwin/goSecretBoxPassword">goSecretBoxPassword</a></td>
<td>A probably paranoid package for securely hashing and encrypting passwords.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/khezen/jwc">jwc</a></td>
<td>JSON Web Cryptography library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xenolf/lego">lego</a></td>
<td>Pure Go ACME client library and CLI tool (for use with Let&rsquo;s Encrypt).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/awnumar/memguard">memguard</a></td>
<td>A pure Go library for handling sensitive values in memory.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kevinburke/nacl">nacl</a></td>
<td>Go implementation of the NaCL set of API&rsquo;s.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hlandau/passlib">passlib</a></td>
<td>Futureproof password hashing library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/unrolled/secure">secure</a></td>
<td>HTTP middleware for Go that facilitates some quick security wins.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/elithrar/simple-scrypt">simple-scrypt</a></td>
<td>Scrypt package with a simple, obvious API and automatic cost calibration built-in.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ssh-vault/ssh-vault">ssh-vault</a></td>
<td>encrypt/decrypt using ssh keys.</td>
</tr>
</tbody>
</table>

<h2>Serialization</h2>

<p><em>Libraries and tools for binary serialization.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/PromonLogicalis/asn1">asn1</a></td>
<td>Asn.1 BER and DER encoding library for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/glycerine/bambam">bambam</a></td>
<td>generator for Cap&rsquo;n Proto schemas from go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pascaldekloe/colfer">colfer</a></td>
<td>Code generation for the Colfer binary format.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jszwec/csvutil">csvutil</a></td>
<td>High Performance, idiomatic CSV record encoding and decoding to native Go structures.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/o1egl/fwencoder">fwencoder</a></td>
<td>Fixed width file parser (encoding and decoding library) for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/glycerine/go-capnproto">go-capnproto</a></td>
<td>Cap&rsquo;n Proto library and parser for go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ugorji/go">go-codec</a></td>
<td>High Performance, feature-Rich, idiomatic encode, decode and rpc library for msgpack, cbor and json, with runtime-based OR code-generation support.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gogo/protobuf">gogoprotobuf</a></td>
<td>Protocol Buffers for Go with Gadgets.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/golang/protobuf">goprotobuf</a></td>
<td>Go support, in the form of a library and protocol compiler plugin, for Google&rsquo;s protocol buffers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/json-iterator/go">jsoniter</a></td>
<td>High-performance 100% compatible drop-in replacement of &ldquo;encoding/json&rdquo;.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mitchellh/mapstructure">mapstructure</a></td>
<td>Go library for decoding generic map values into native Go structures.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yvasiyarov/php_session_decoder">php_session_decoder</a></td>
<td>GoLang library for working with PHP session format and PHP Serialize/Unserialize functions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tuvistavie/structomap">structomap</a></td>
<td>Library to easily and dynamically generate maps from static structures.</td>
</tr>
</tbody>
</table>

<h2>Server Applications</h2>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xyproto/algernon">algernon</a></td>
<td>HTTP/2 web server with built-in support for Lua, Markdown, GCSS and Amber.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mholt/caddy">Caddy</a></td>
<td>Caddy is an alternative, HTTP/2 web server that&rsquo;s easy to configure and use.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.consul.io/">consul</a></td>
<td>Consul is a tool for service discovery, monitoring and configuration.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cortesi/devd">devd</a></td>
<td>Local webserver for developers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Bilibili/discovery">discovery</a></td>
<td>A registry for resilient mid-tier load balancing and failover.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/coreos/etcd">etcd</a></td>
<td>Highly-available key value store for shared configuration and service discovery.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/getfider/fider">Fider</a></td>
<td>Fider is an open platform to collect and organize customer feedback.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/checkr/flagr">Flagr</a></td>
<td>Flagr is an open-source feature flagging and A/B testing service.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ortuman/jackal">jackal</a></td>
<td>An XMPP server written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/minio/minio">minio</a></td>
<td>Minio is a distributed object storage server.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://nsq.io/">nsq</a></td>
<td>A realtime distributed messaging platform.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/spiral/roadrunner">RoadRunner</a></td>
<td>High-performance PHP application server, load-balancer and process manager.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://git.sci4me.com/sci4me/yakvs">yakvs</a></td>
<td>Small, networked, in-memory key-value store.</td>
</tr>
</tbody>
</table>

<h2>Template Engines</h2>

<p><em>Libraries and tools for templating and lexing.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yosssi/ace">ace</a></td>
<td>Ace is an HTML template engine for Go, inspired by Slim and Jade. Ace is a refinement of Gold.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/eknkc/amber">amber</a></td>
<td>Amber is an elegant templating engine for Go Programming Language It is inspired from HAML and Jade.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dskinner/damsel">damsel</a></td>
<td>Markup language featuring html outlining via css-selectors, extensible via pkg html/template and others.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/benbjohnson/ego">ego</a></td>
<td>Lightweight templating language that lets you write templates in Go. Templates are translated into Go and compiled.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dannyvankooten/extemplate">extemplate</a></td>
<td>Tiny wrapper around html/template to allow for easy file-based template inheritance.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/valyala/fasttemplate">fasttemplate</a></td>
<td>Simple and fast template engine. Substitutes template placeholders up to 10x faster than <a href="http://golang.org/pkg/text/template/">text/template</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jung-kurt/gofpdf">gofpdf</a></td>
<td>PDF document generator with high level support for text, drawing and images.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shiyanhui/hero">hero</a></td>
<td>Hero is a handy, fast and powerful go template engine.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/CloudyKit/jet">jet</a></td>
<td>Jet template engine.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ziutek/kasia.go">kasia.go</a></td>
<td>Templating system for HTML and other text documents - go implementation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/osteele/liquid">liquid</a></td>
<td>Go implementation of Shopify Liquid templates.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hoisie/mustache">mustache</a></td>
<td>Go implementation of the Mustache template language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/flosch/pongo2">pongo2</a></td>
<td>Django-like template-engine for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/valyala/quicktemplate">quicktemplate</a></td>
<td>Fast, powerful, yet easy to use template engine. Converts templates into Go code and then compiles it.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aymerick/raymond">raymond</a></td>
<td>Complete handlebars implementation in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sipin/gorazor">Razor</a></td>
<td>Razor view engine for Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/robfig/soy">Soy</a></td>
<td>Closure templates (aka Soy templates) for Go, following the <a href="https://developers.google.com/closure/templates/">official spec</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gobuffalo/velvet">velvet</a></td>
<td>Complete handlebars implementation in Go.</td>
</tr>
</tbody>
</table>

<h2>Testing</h2>

<p><em>Libraries for testing codebases and generating test data.</em></p>

<ul>
<li>Testing Frameworks</li>
</ul>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-playground/assert">assert</a></td>
<td>Basic Assertion Library used along side native go testing, with building blocks for custom assertions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cavaliercoder/badio">badio</a></td>
<td>Extensions to Go&rsquo;s <code>testing/iotest</code> package.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/h2non/baloo">baloo</a></td>
<td>Expressive and versatile end-to-end HTTP API testing made easy.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fulldump/biff">biff</a></td>
<td>Bifurcation testing framework, BDD compatible.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/marioidival/bro">bro</a></td>
<td>Watch files in directory and run tests for them.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/percolate/charlatan">charlatan</a></td>
<td>Tool to generate fake interface implementations for tests.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bradleyjkemp/cupaloy">cupaloy</a></td>
<td>Simple snapshot testing addon for your test framework.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/khaiql/dbcleaner">dbcleaner</a></td>
<td>Clean database for testing purpose, inspired by <code>database_cleaner</code> in Ruby.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/viant/dsunit">dsunit</a></td>
<td>Datastore testing for SQL, NoSQL, structured files.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/viant/endly">endly</a></td>
<td>Declarative end to end functional testing.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/verdverm/frisby">frisby</a></td>
<td>REST API testing framework.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://onsi.github.io/ginkgo/">ginkgo</a></td>
<td>BDD Testing Framework for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/msoap/go-carpet">go-carpet</a></td>
<td>Tool for viewing test coverage in terminal.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/google/go-cmp">go-cmp</a></td>
<td>Package for comparing Go values in tests.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zimmski/go-mutesting">go-mutesting</a></td>
<td>Mutation testing for Go source code.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/maxatome/go-testdeep">go-testdeep</a></td>
<td>Extremely flexible golang deep comparison, extends the go testing package.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dnaeon/go-vcr">go-vcr</a></td>
<td>Record and replay your HTTP interactions for fast, deterministic and accurate tests.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/franela/goblin">goblin</a></td>
<td>Mocha like testing framework fo Go.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://labix.org/gocheck">gocheck</a></td>
<td>More advanced testing framework alternative to gotest.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/smartystreets/goconvey/">GoConvey</a></td>
<td>BDD-style framework with web UI and live reload.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/corbym/gocrest">gocrest</a></td>
<td>Composable hamcrest-like matchers for Go assertions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/DATA-DOG/godog">godog</a></td>
<td>Cucumber or Behat like BDD framework for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/appleboy/gofight">gofight</a></td>
<td>API Handler Testing for Golang Router framework.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/corbym/gogiven">gogiven</a></td>
<td>YATSPEC-like BDD testing framework for Go.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://onsi.github.io/gomega/">gomega</a></td>
<td>Rspec like matcher/assertion library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/orfjackal/gospec">GoSpec</a></td>
<td>BDD-style testing framework for the Go programming language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/stesla/gospecify">gospecify</a></td>
<td>This provides a BDD syntax for testing your Go code. It should be familiar to anybody who has used libraries such as rspec.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pavlo/gosuite">gosuite</a></td>
<td>Brings lightweight test suites with setup/teardown facilities to <code>testing</code> by leveraging Go1.7&rsquo;s Subtests.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gotestyourself/gotest.tools">gotest.tools</a></td>
<td>A collection of packages to augment the go testing package and support common patterns.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rdrdr/hamcrest">Hamcrest</a></td>
<td>fluent framework for declarative Matcher objects that, when applied to input values, produce self-describing results.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gavv/httpexpect">httpexpect</a></td>
<td>Concise, declarative, and easy to use end-to-end HTTP and REST API testing.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yookoala/restit">restit</a></td>
<td>Go micro framework to help writing RESTful API integration test.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-testfixtures/testfixtures">testfixtures</a></td>
<td>A helper for Rails&rsquo; like test fixtures to test database applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/stretchr/testify">Testify</a></td>
<td>Sacred extension to the standard go testing package.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zhulongcheng/testsql">testsql</a></td>
<td>Generate test data from SQL files before testing and clear it after finished.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/vcaesar/tt">Tt</a></td>
<td>Simple and colorful test tools.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/posener/wstest">wstest</a></td>
<td>Websocket client for unit-testing a websocket http.Handler.</td>
</tr>
</tbody>
</table>

<ul>
<li>Mock</li>
</ul>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/maxbrunsfeld/counterfeiter">counterfeiter</a></td>
<td>Tool for generating self-contained mock objects.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/DATA-DOG/go-sqlmock">go-sqlmock</a></td>
<td>Mock SQL driver for testing database interactions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/DATA-DOG/go-txdb">go-txdb</a></td>
<td>Single transaction based database driver mainly for testing purposes.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/h2non/gock">gock</a></td>
<td>Versatile HTTP mocking made easy.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/golang/mock">gomock</a></td>
<td>Mocking framework for the Go programming language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/seborama/govcr">govcr</a></td>
<td>HTTP mock for Golang: record and replay HTTP interactions for offline testing.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/SpectoLabs/hoverfly">hoverfly</a></td>
<td>HTTP(S) proxy for recording and simulating REST/SOAP APIs with extensible middleware and easy-to-use CLI.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gojuno/minimock">minimock</a></td>
<td>Mock generator for Go interfaces.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tv42/mockhttp">mockhttp</a></td>
<td>Mock object for Go http.ResponseWriter.</td>
</tr>
</tbody>
</table>

<ul>
<li>Fuzzing and delta-debugging/reducing/shrinking.</li>
</ul>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dvyukov/go-fuzz">go-fuzz</a></td>
<td>Randomized testing system.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/google/gofuzz">gofuzz</a></td>
<td>Library for populating go objects with random values.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zimmski/tavor">Tavor</a></td>
<td>Generic fuzzing and delta-debugging framework.</td>
</tr>
</tbody>
</table>

<ul>
<li>Selenium and browser control tools.</li>
</ul>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mafredri/cdp">cdp</a></td>
<td>Type-safe bindings for the Chrome Debugging Protocol that can be used with browsers or other debug targets that implement it.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/knq/chromedp">chromedp</a></td>
<td>a way to drive/test Chrome, Safari, Edge, Android Webviews, and other browsers supporting the Chrome Debugging Protocol.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aerokube/ggr">ggr</a></td>
<td>a lightweight server that routes and proxies Selenium Wedriver requests to multiple Selenium hubs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aerokube/selenoid">selenoid</a></td>
<td>alternative Selenium hub server that launches browsers within containers.</td>
</tr>
</tbody>
</table>

<h2>Text Processing</h2>

<p><em>Libraries for parsing and manipulating texts.</em></p>

<ul>
<li>Specific Formats</li>
</ul>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Guitarbum722/align">align</a></td>
<td>A general purpose application that aligns text.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sbstjn/allot">allot</a></td>
<td>Placeholder and wildcard text parsing for CLI tools and bots.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/CalebQ42/bbConvert">bbConvert</a></td>
<td>Converts bbCode to HTML that allows you to add support for custom bbCode tags.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/russross/blackfriday">blackfriday</a></td>
<td>Markdown processor in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/microcosm-cc/bluemonday">bluemonday</a></td>
<td>HTML Sanitizer.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/asciimoo/colly">colly</a></td>
<td>Fast and Elegant Scraping Framework for Gophers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mingrammer/commonregex">commonregex</a></td>
<td>A collection of common regular expressions for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/slotix/dataflowkit">dataflowkit</a></td>
<td>Web scraping Framework to turn websites into structured data.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ockam-network/did">did</a></td>
<td>DID (Decentralized Identifiers) Parser and Stringer in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hscells/doi">doi</a></td>
<td>Document object identifier (doi) parser in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/editorconfig/editorconfig-core-go">editorconfig-core-go</a></td>
<td>Editorconfig file parser and manipulator for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/endeveit/enca">enca</a></td>
<td>Minimal cgo bindings for <a href="http://cihar.com/software/enca/">libenca</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mickep76/encdec">encdec</a></td>
<td>Package provides a generic interface to encoders and decodersa.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/alixaxel/genex">genex</a></td>
<td>Count and expand Regular Expressions into all matching Strings.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://godoc.org/github.com/shurcooL/github_flavored_markdown">github_flavored_markdown</a></td>
<td>GitHub Flavored Markdown renderer (using blackfriday) with fenced code block highlighting, clickable header anchor links.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ianlopshire/go-fixedwidth">go-fixedwidth</a></td>
<td>Fixed-width text formatting (encoder/decoder with reflection).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dustin/go-humanize">go-humanize</a></td>
<td>Formatters for time, numbers, and memory size to human readable format.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/adrianmo/go-nmea">go-nmea</a></td>
<td>NMEA parser library for the Go language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mattn/go-runewidth">go-runewidth</a></td>
<td>Functions to get fixed width of the character or string.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mozillazg/go-slugify">go-slugify</a></td>
<td>Make pretty slug with multiple languages support.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/emersion/go-vcard">go-vcard</a></td>
<td>Parse and format vCard.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/trubitsyn/go-zero-width">go-zero-width</a></td>
<td>Zero-width character detection and removal for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mmcdole/gofeed">gofeed</a></td>
<td>Parse RSS and Atom feeds in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/awalterschulze/gographviz">gographviz</a></td>
<td>Parses the Graphviz DOT language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/labstack/gommon/tree/master/bytes">gommon/bytes</a></td>
<td>Format bytes to string.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/polera/gonameparts">gonameparts</a></td>
<td>Parses human names into individual name parts.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/andrewstuart/goq">goq</a></td>
<td>Declarative unmarshaling of HTML using struct tags with jQuery syntax (uses GoQuery).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/PuerkitoBio/goquery">GoQuery</a></td>
<td>GoQuery brings a syntax and a set of features similar to jQuery to the Go language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zach-klippenstein/goregen">goregen</a></td>
<td>Library for generating random strings from regular expressions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/leonelquinteros/gotext">gotext</a></td>
<td>GNU gettext utilities for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/endeveit/guesslanguage">guesslanguage</a></td>
<td>Functions to determine the natural language of a unicode text.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/antchfx/htmlquery">htmlquery</a></td>
<td>An XPath query package for HTML, lets you extract data or evaluate from HTML documents by an XPath expression.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/facebookgo/inject">inject</a></td>
<td>Package inject provides a reflect based injector.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/clbanning/mxj">mxj</a></td>
<td>Encode / decode XML as JSON or map[string]interface{}; extract values with dot-notation paths and wildcards. Replaces x2j and j2x packages.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gortc/sdp">sdp</a></td>
<td>SDP: Session Description Protocol [<a href="https://tools.ietf.org/html/rfc4566">RFC 4566</a>].</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mvdan/sh">sh</a></td>
<td>Shell parser and formatter.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gosimple/slug">slug</a></td>
<td>URL-friendly slugify with multiple languages support.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/avelino/slugify">Slugify</a></td>
<td>Go slugify application that handles string.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zhengchun/syndfeed">syndfeed</a></td>
<td>A syndication feed for Atom 1.0 and RSS 2.0.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/BurntSushi/toml">toml</a></td>
<td>TOML configuration format (encoder/decoder with reflection).</td>
</tr>
</tbody>
</table>

<ul>
<li>Utility</li>
</ul>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/JoshuaDoes/gofuckyourself">gofuckyourself</a></td>
<td>A sanitization-based swear filter for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bndr/gotabulate">gotabulate</a></td>
<td>Easily pretty-print your tabular data with Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/codemodus/kace">kace</a></td>
<td>Common case conversions covering common initialisms.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nproc/parseargs-go">parseargs-go</a></td>
<td>string argument parser that understands quotes and backslashes.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/codemodus/parth">parth</a></td>
<td>URL path segmentation parsing.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yourbasic/radix">radix</a></td>
<td>fast string sorting algorithm.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/stackerzzq/xj2go">xj2go</a></td>
<td>Convert xml or json to go struct.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mvdan/xurls">xurls</a></td>
<td>Extract urls from text.</td>
</tr>
</tbody>
</table>

<h2>Third-party APIs</h2>

<p><em>Libraries for accessing third party APIs.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ngs/go-amazon-product-advertising-api">amazon-product-advertising-api</a></td>
<td>Go Client Library for <a href="https://affiliate-program.amazon.com/gp/advertising/api/detail/main.html">Amazon Product Advertising API</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ChimeraCoder/anaconda">anaconda</a></td>
<td>Go client library for the Twitter 1.1 API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aws/aws-sdk-go">aws-sdk-go</a></td>
<td>The official AWS SDK for the Go programming language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/naegelejd/brewerydb">brewerydb</a></td>
<td>Go library for accessing the BreweryDB API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/andygrunwald/cachet">cachet</a></td>
<td>Go client library for <a href="https://cachethq.io/">Cachet (open source status page system)</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jszwedko/go-circleci">circleci</a></td>
<td>Go client library for interacting with CircleCI&rsquo;s API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/samuelcouch/clarifai">clarifai</a></td>
<td>Go client library for interfacing with the Clarifai API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/codeship/codeship-go">codeship-go</a></td>
<td>Go client library for interacting with Codeship&rsquo;s API v2.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/coinpaprika/coinpaprika-api-go-client">coinpaprika-go</a></td>
<td>Go client library for interacting with Coinpaprika&rsquo;s API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bwmarrin/discordgo">discordgo</a></td>
<td>Go bindings for the Discord Chat API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/onrik/ethrpc">ethrpc</a></td>
<td>Go bindings for Ethereum JSON RPC API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/huandu/facebook">facebook</a></td>
<td>Go Library that supports the Facebook Graph API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/maddevsio/fcm">fcm</a></td>
<td>Go library for Firebase Cloud Messaging.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/emiddleton/gads">gads</a></td>
<td>Google Adwords Unofficial API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bit4bit/gami">gami</a></td>
<td>Go library for Asterisk Manager Interface.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Aorioli/gcm">gcm</a></td>
<td>Go library for Google Cloud Messaging.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/codingsince1985/geo-golang">geo-golang</a></td>
<td>Go Library to access <a href="https://developers.google.com/maps/documentation/geocoding/intro">Google Maps</a>, <a href="http://open.mapquestapi.com/geocoding/">MapQuest</a>, <a href="https://developer.mapquest.com/documentation/open/nominatim-search">Nominatim</a>, <a href="http://geocoder.opencagedata.com/api.html">OpenCage</a>, <a href="https://msdn.microsoft.com/en-us/library/ff701715.aspx">Bing</a>, <a href="https://www.mapbox.com/developers/api/geocoding/">Mapbox</a>, and <a href="https://wiki.openstreetmap.org/wiki/Nominatim">OpenStreetMap</a> geocoding / reverse geocoding APIs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/google/go-github">github</a></td>
<td>Go library for accessing the GitHub REST API v3.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shurcooL/githubql">githubql</a></td>
<td>Go library for accessing the GitHub GraphQL API v4.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/axelspringer/go-chronos">go-chronos</a></td>
<td>Go library for interacting with the <a href="https://mesos.github.io/chronos/">Chronos</a> Job Scheduler</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/PaulRosset/go-hacknews">go-hacknews</a></td>
<td>Tiny Go client for HackerNews API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/koffeinsource/go-imgur">go-imgur</a></td>
<td>Go client library for <a href="https://imgur.com">imgur</a></td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/andygrunwald/go-jira">go-jira</a></td>
<td>Go client library for <a href="https://www.atlassian.com/software/jira">Atlassian JIRA</a></td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gambol99/go-marathon">go-marathon</a></td>
<td>Go library for interacting with Mesosphere&rsquo;s Marathon PAAS.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nstratos/go-myanimelist">go-myanimelist</a></td>
<td>Go client library for accessing the <a href="http://myanimelist.net/modules.php?go=api">MyAnimeList API</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/esurdam/go-sophos">go-sophos</a></td>
<td>Go client library for the <a href="https://www.sophos.com/en-us/medialibrary/PDFs/documentation/UTMonAWS/Sophos-UTM-RESTful-API.pdf?la=en">Sophos UTM REST API</a> with zero dependencies.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sergioaugrod/go-sptrans">go-sptrans</a></td>
<td>Go client library for the SPTrans Olho Vivo API.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://gitlab.com/toby3d/telegraph">go-telegraph</a></td>
<td>Telegraph publishing platform API client.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/andygrunwald/go-trending">go-trending</a></td>
<td>Go library for accessing <a href="https://github.com/trending">trending repositories</a> and <a href="https://github.com/trending/developers">developers</a> at Github.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/knspriggs/go-twitch">go-twitch</a></td>
<td>Go client for interacting with the Twitch v3 API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dghubble/go-twitter">go-twitter</a></td>
<td>Go client library for the Twitter v1.1 APIs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hbagdi/go-unsplash">go-unsplash</a></td>
<td>Go client library for the <a href="https://unsplash.com">Unsplash.com</a> API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nishanths/go-xkcd">go-xkcd</a></td>
<td>Go client for the xkcd API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mamal72/golyrics">golyrics</a></td>
<td>Golyrics is a Go library to fetch music lyrics data from the Wikia website.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/michiwend/gomusicbrainz">GoMusicBrainz</a></td>
<td>Go MusicBrainz WS2 client library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/google/google-api-go-client">google</a></td>
<td>Auto-generated Google APIs for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/chonthu/go-google-analytics">google-analytics</a></td>
<td>Simple wrapper for easy google analytics reporting.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/GoogleCloudPlatform/gcloud-golang">google-cloud</a></td>
<td>Google Cloud APIs Go Client Library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ngs/go-google-email-audit-api">google-email-audit-api</a></td>
<td>Go client library for <a href="https://developers.google.com/admin-sdk/email-audit/">Google G Suite Email Audit API</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jsgilmore/gostorm">gostorm</a></td>
<td>GoStorm is a Go library that implements the communications protocol required to write Storm spouts and Bolts in Go that communicate with the Storm shells.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/andybons/hipchat">hipchat</a></td>
<td>This project implements a golang client library for the Hipchat API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/daneharrigan/hipchat">hipchat (xmpp)</a></td>
<td>A golang package to communicate with HipChat over XMPP.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Henry-Sarabia/igdb">igdb</a></td>
<td>Go client for the <a href="https://api.igdb.com/">Internet Game Database API</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Medium/medium-sdk-go">Medium</a></td>
<td>Golang SDK for Medium&rsquo;s OAuth2 API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/andygrunwald/megos">megos</a></td>
<td>Client library for accessing an <a href="http://mesos.apache.org/">Apache Mesos</a> cluster.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/minio/minio-go">minio-go</a></td>
<td>Minio Go Library for Amazon S3 compatible cloud storage.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dukex/mixpanel">mixpanel</a></td>
<td>Mixpanel is a library for tracking events and sending Mixpanel profile updates to Mixpanel from your go applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mxpv/patreon-go">patreon-go</a></td>
<td>Go library for Patreon API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/logpacker/PayPal-Go-SDK">paypal</a></td>
<td>Wrapper for PayPal payment API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/playlyfe/playlyfe-go-sdk">playlyfe</a></td>
<td>The Playlyfe Rest API Go SDK.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gregdel/pushover">pushover</a></td>
<td>Go wrapper for the Pushover API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Omie/rrdaclient">rrdaclient</a></td>
<td>Go Library to access statdns.com API, which is in turn RRDA API. DNS Queries over HTTP.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rapito/go-shopify">shopify</a></td>
<td>Go Library to make CRUD request to the Shopify API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rhnvrm/simples3">simples3</a></td>
<td>Simple no frills AWS S3 Library using REST with V4 Signing written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nlopes/slack">slack</a></td>
<td>Slack API in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sergiotapia/smitego">smite</a></td>
<td>Go package to wraps access to the Smite game API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rapito/go-spotify">spotify</a></td>
<td>Go Library to access Spotify WEB API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sostronk/go-steam">steam</a></td>
<td>Go Library to interact with Steam game servers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/stripe/stripe-go">stripe</a></td>
<td>Go client for the Stripe API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dietsche/textbelt">textbelt</a></td>
<td>Go client for the textbelt.com txt messaging API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jbrodriguez/go-tmdb">TheMovieDb</a></td>
<td>Simple golang package to communicate with <a href="https://themoviedb.org">themoviedb.org</a>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/poorny/translate">translate</a></td>
<td>Go online translation package.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/adlio/trello">Trello</a></td>
<td>Go wrapper for the Trello API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mattcunningham/gumblr">tumblr</a></td>
<td>Go wrapper for the Tumblr v2 API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bitfield/uptimerobot">uptimerobot</a></td>
<td>Go wrapper and command-line client for the Uptime Robot v2 API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-playground/webhooks">webhooks</a></td>
<td>Webhook receiver for GitHub and Bitbucket.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/wit-ai/wit-go">wit-go</a></td>
<td>Go client for wit.ai HTTP API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/brunomvsouza/ynab.go">ynab</a></td>
<td>Go wrapper for the YNAB API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gojuno/go-zooz">zooz</a></td>
<td>Go client for the Zooz API.</td>
</tr>
</tbody>
</table>

<h2>Utilities</h2>

<p><em>General utilities and tools to make your life easier.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bahlo/abutil">abutil</a></td>
<td>Collection of often-used Golang helpers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/topfreegames/apm">apm</a></td>
<td>Process manager for Golang applications with an HTTP API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/icza/backscanner">backscanner</a></td>
<td>A scanner similar to bufio.Scanner, but it reads and returns lines in reverse order, starting at a given position and going backward.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tmrts/boilr">boilr</a></td>
<td>Blazingly fast CLI tool for creating projects from boilerplate templates.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/antham/chyle">chyle</a></td>
<td>Changelog generator using a git repository with multiple configuration possibilities.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cep21/circuit">circuit</a></td>
<td>An efficient and feature complete Hystrix like Go implementation of the circuit breaker pattern.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rubyist/circuitbreaker">circuitbreaker</a></td>
<td>Circuit Breakers in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jonboulle/clockwork">clockwork</a></td>
<td>A simple fake clock for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/txgruppi/command">command</a></td>
<td>Command pattern for Go with thread safe serial and parallel dispatcher.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jutkko/copy-pasta">copy-pasta</a></td>
<td>Universal multi-workstation clipboard that uses S3 like backend for the storage.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bcicen/ctop">ctop</a></td>
<td><a href="http://ctop.sh">Top-like</a> interface (e.g. htop) for container metrics.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/posener/ctxutil">ctxutil</a></td>
<td>A collection of utility functions for contexts.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/vrecan/death">Death</a></td>
<td>Managing go application shutdown with signals.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ulule/deepcopier">Deepcopier</a></td>
<td>Simple struct copying for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/derekparker/delve">delve</a></td>
<td>Go debugger.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kirillDanshin/dlog">dlog</a></td>
<td>Compile-time controlled logger to make your release smaller without removing debug calls.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cristianoliveira/ergo">ergo</a></td>
<td>The management of multiple local services running over different ports made easy.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nullne/evaluator">evaluator</a></td>
<td>Evaluate an expression dynamicly based on s-expression. It&rsquo;s simple and easy to extend.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/digitalcrab/fastlz">fastlz</a></td>
<td>Wrap over <a href="http://fastlz.org/">FastLz</a> (free, open-source, portable real-time compression library) for GoLang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/h2non/filetype">filetype</a></td>
<td>Small package to infer the file type checking the magic numbers signature.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yaronsumel/filler">filler</a></td>
<td>small utility to fill structs using &ldquo;fill&rdquo; tag.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gookit/filter">filter</a></td>
<td>provide filtering, sanitizing, and conversion of Go data.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/junegunn/fzf">fzf</a></td>
<td>Command-line fuzzy finder written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/maxcnunes/gaper">gaper</a></td>
<td>Builds and restarts a Go project when it crashes or some watched file changes.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-playground/generate">generate</a></td>
<td>runs go generate recursively on a specified path or environment variable and can filter by regex.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/git-time-metric/gtm">git-time-metric</a></td>
<td>Simple, seamless, lightweight time tracking for Git.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/asticode/go-astitodo">go-astitodo</a></td>
<td>Parse TODOs in your GO code.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/wendigo/go-bind-plugin">go-bind-plugin</a></td>
<td>go:generate tool for wrapping symbols exported by golang plugins (1.8 only).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ungerik/go-dry">go-dry</a></td>
<td>DRY (don&rsquo;t repeat yourself) package for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/thoas/go-funk">go-funk</a></td>
<td>Modern Go utility library which provides helpers (map, find, contains, filter, chunk, reverse, &hellip;).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Talento90/go-health">go-health</a></td>
<td>Health package simplifies the way you add health check to your services.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mozillazg/go-httpheader">go-httpheader</a></td>
<td>Go library for encoding structs into Header fields.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/beefsack/go-rate">go-rate</a></td>
<td>Timed rate limiter for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ikeikeikeike/go-sitemap-generator">go-sitemap-generator</a></td>
<td>XML Sitemap generator written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/uber/go-torch">go-torch</a></td>
<td>Stochastic flame graph profiler for Go programs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sadlil/go-trigger">go-trigger</a></td>
<td>Go-lang global event triggerer, Register Events with an id and trigger the event from anywhere from your project.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/carlescere/goback">goback</a></td>
<td>Go simple exponential backoff package.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/VividCortex/godaemon">godaemon</a></td>
<td>Utility to write daemons.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dropbox/godropbox">godropbox</a></td>
<td>Common libraries for writing Go services/applications from Dropbox.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cosiner/gohper">gohper</a></td>
<td>Various tools/modules help for development.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/msempere/golarm">golarm</a></td>
<td>Fire alarms with system events.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mlimaloureiro/golog">golog</a></td>
<td>Easy and lightweight CLI tool to time track your tasks.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bndr/gopencils">gopencils</a></td>
<td>Small and simple package to easily consume REST APIs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/michiwend/goplaceholder">goplaceholder</a></td>
<td>a small golang lib to generate placeholder images.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/goreleaser/goreleaser">goreleaser</a></td>
<td>Deliver Go binaries as fast and easily as possible.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/wgliang/goreporter">goreporter</a></td>
<td>Golang tool that does static analysis, unit testing, code review and generate code quality report.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/linxGnu/goseaweedfs">goseaweedfs</a></td>
<td>SeaweedFS client library with almost full features.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ik5/gostrutils">gostrutils</a></td>
<td>Collections of string manipulation and conversion functions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/subosito/gotenv">gotenv</a></td>
<td>Load environment variables from <code>.env</code> or any <code>io.Reader</code> in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tenntenn/gpath">gpath</a></td>
<td>Library to simplify access struct fields with Go&rsquo;s expression in reflection.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://gubrak.github.io/">gubrak</a></td>
<td>Golang utility library with syntactic sugar. It&rsquo;s like lodash, but for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/htcat/htcat">htcat</a></td>
<td>Parallel and Pipelined HTTP GET Utility.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/github/hub">hub</a></td>
<td>wrap git commands with additional functionality to interact with github from the terminal.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/afex/hystrix-go">hystrix-go</a></td>
<td>Implements Hystrix patterns of programmer-defined fallbacks aka circuit breaker.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/immortal/immortal">immortal</a></td>
<td>*nix cross-platform (OS agnostic) supervisor.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mengzhuo/intrinsic">intrinsic</a></td>
<td>Use x86 SIMD without writing any assembly code.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/wesovilabs/koazee">koazee</a></td>
<td>Library inspired in Lazy evaluation and functional programming that takes the hassle out of working with arrays.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jaschaephraim/lrserver">lrserver</a></td>
<td>LiveReload server for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/minio/mc">mc</a></td>
<td>Minio Client provides minimal tools to work with Amazon S3 compatible cloud storage and filesystems.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/imdario/mergo">mergo</a></td>
<td>Helper to merge structs and maps in Golang. Useful for configuration default values, avoiding messy if-statements.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zRedShift/mimemagic">mimemagic</a></td>
<td>Pure Go ultra performant MIME sniffing library/utility.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aofei/mimesniffer">mimesniffer</a></td>
<td>A MIME type sniffer for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gabriel-vasile/mimetype">mimetype</a></td>
<td>Package for MIME type detection based on magic numbers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tdewolff/minify">minify</a></td>
<td>Fast minifiers for HTML, CSS, JS, XML, JSON and SVG file formats.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/icza/minquery">minquery</a></td>
<td>MongoDB / mgo.v2 query that supports efficient pagination (cursors to continue listing documents where we left off).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tj/mmake">mmake</a></td>
<td>Modern Make.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/StabbyCutyou/moldova">moldova</a></td>
<td>Utility for generating random data based on an input template.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/davrodpin/mole">mole</a></td>
<td>cli app to easily create ssh tunnels.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/linxGnu/mssqlx">mssqlx</a></td>
<td>Database client library, proxy for any master slave, master master structures. Lightweight and auto balancing in mind.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/VividCortex/multitick">multitick</a></td>
<td>Multiplexor for aligned tickers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/inancgumus/myhttp">myhttp</a></td>
<td>Simple API to make HTTP GET requests with timeout support.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/e-dard/netbug">netbug</a></td>
<td>Easy remote profiling of your services.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xta/okrun">okrun</a></td>
<td>go run error steamroller.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/btnguyen2k/olaf">olaf</a></td>
<td>Twitter Snowflake implemented in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/adelowo/onecache">onecache</a></td>
<td>Caching library with support for multiple backend stores (Redis, Memcached, filesystem etc).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/maruel/panicparse">panicparse</a></td>
<td>Groups similar goroutines and colorizes stack dump.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/peco/peco">peco</a></td>
<td>Simplistic interactive filtering tool.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/VividCortex/pm">pm</a></td>
<td>Process (i.e. goroutine) manager with an HTTP API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pkg/profile">profile</a></td>
<td>Simple profiling support package for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zpatrick/rclient">rclient</a></td>
<td>Readable, flexible, simple-to-use client for REST APIs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tockins/realize">realize</a></td>
<td>Go build system with file watchers and live reload. Run, build and watch file changes with custom paths.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ssgreg/repeat">repeat</a></td>
<td>Go implementation of different backoff strategies useful for retrying operations and heartbeating.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mozillazg/request">request</a></td>
<td>Go HTTP Requests for Humans™.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/abo/rerate">rerate</a></td>
<td>Redis-based rate counter and rate limiter for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ivpusic/rerun">rerun</a></td>
<td>Recompiling and rerunning go apps when source changes.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-resty/resty">resty</a></td>
<td>Simple HTTP and REST client for Go inspired by Ruby rest-client.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kamilsk/retry">retry</a></td>
<td>Functional mechanism based on context to perform actions repetitively until successful.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/percolate/retry">retry</a></td>
<td>A simple but highly configurable retry package for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/thedevsaddam/retry">retry</a></td>
<td>Simple and easy retry mechanism package for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shafreeck/retry">retry</a></td>
<td>A pretty simple library to ensure your work to be done.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rafaeljesus/retry-go">retry-go</a></td>
<td>Retrying made simple and easy for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/VividCortex/robustly">robustly</a></td>
<td>Runs functions resiliently, catching and restarting panics.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/syntaqx/serve">serve</a></td>
<td>A static http server anywhere you need.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/leaanthony/slicer">slicer</a></td>
<td>Makes working with slices easier.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/briandowns/spinner">spinner</a></td>
<td>Go package to easily provide a terminal spinner with options.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jmoiron/sqlx">sqlx</a></td>
<td>provides a set of extensions on top of the excellent built-in database/sql package.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yaa110/sslice">sslice</a></td>
<td>Create a slice which is always sorted.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/asdine/storm">Storm</a></td>
<td>Simple and powerful toolkit for BoltDB.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/PumpkinSeed/structs">structs</a></td>
<td>Implement simple functions to manipulate structs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-task/task">Task</a></td>
<td>simple &ldquo;Make&rdquo; alternative.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/viant/toolbox">toolbox</a></td>
<td>Slice, map, multimap, struct, function, data conversion utilities. Service router, macro evaluator, tokenizer.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/alxrm/ugo">ugo</a></td>
<td>ugo is slice toolbox with concise syntax for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/esemplastic/unis">UNIS</a></td>
<td>Common Architecture™ for String Utilities in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/knq/usql">usql</a></td>
<td>usql is a universal command-line interface for SQL databases.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shomali11/util">util</a></td>
<td>Collection of useful utility functions. (strings, concurrency, manipulations, &hellip;).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/asciimoo/wuzz">wuzz</a></td>
<td>Interactive cli tool for HTTP inspection.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/monmohan/xferspdy">xferspdy</a></td>
<td>Xferspdy provides binary diff and patch library in golang.</td>
</tr>
</tbody>
</table>

<h2>UUID</h2>

<p><em>Libraries for working with UUIDs.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jakehl/goid">goid</a></td>
<td>Generate and Parse RFC4122 compliant V4 UUIDs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/agext/uuid">uuid</a></td>
<td>Generate, encode, and decode UUIDs v1 with fast or cryptographic-quality random node identifier.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gofrs/uuid">uuid</a></td>
<td>Implementation of Universally Unique Identifier (UUID). Supports both creation and parsing of UUIDs. Actively maintained fork of satori uuid.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/edwingeng/wuid">wuid</a></td>
<td>An extremely fast unique number generator, 10-135 times faster than UUID.</td>
</tr>
</tbody>
</table>

<h2>Validation</h2>

<p><em>Libraries for validation.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/asaskevich/govalidator">govalidator</a></td>
<td>Validators and sanitizers for strings, numerics, slices and structs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/thedevsaddam/govalidator">govalidator</a></td>
<td>Validate Golang request data with simple rules. Highly inspired by Laravel&rsquo;s request validation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-ozzo/ozzo-validation">ozzo-validation</a></td>
<td>Supports validation of various data types (structs, strings, maps, slices, etc.) with configurable and extensible validation rules specified in usual code constructs instead of struct tags.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gookit/validate">validate</a></td>
<td>Go package for data validation and filtering. support validate Map, Struct, Request(Form, JSON, url.Values, Uploaded Files) data and more features.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gobuffalo/validate">validate</a></td>
<td>This package provides a framework for writing validations for Go applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-playground/validator">validator</a></td>
<td>Go Struct and Field validation, including Cross Field, Cross Struct, Map, Slice and Array diving.</td>
</tr>
</tbody>
</table>

<h2>Version Control</h2>

<p><em>Libraries for version control.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rjeczalik/gh">gh</a></td>
<td>Scriptable server and net/http middleware for GitHub Webhooks.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/libgit2/git2go">git2go</a></td>
<td>Go bindings for libgit2.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sourcegraph/go-vcs">go-vcs</a></td>
<td>manipulate and inspect VCS repositories in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/beyang/hgo">hgo</a></td>
<td>Hgo is a collection of Go packages providing read-access to local Mercurial repositories.</td>
</tr>
</tbody>
</table>

<h2>Video</h2>

<p><em>Libraries for manipulating video.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/3d0c/gmf">gmf</a></td>
<td>Go bindings for FFmpeg av* libraries.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/asticode/go-astisub">go-astisub</a></td>
<td>Manipulate subtitles in GO (.srt, .stl, .ttml, .webvtt, .ssa/.ass, teletext, .smi, etc.).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/asticode/go-astits">go-astits</a></td>
<td>Parse and demux MPEG Transport Streams (.ts) natively in GO.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/quangngotan95/go-m3u8">go-m3u8</a></td>
<td>Parser and generator library for Apple m3u8 playlists.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/giorgisio/goav">goav</a></td>
<td>Comphrensive Go bindings for FFmpeg.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ziutek/gst">gst</a></td>
<td>Go bindings for GStreamer.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/wargarblgarbl/libgosubs">libgosubs</a></td>
<td>Subtitle format support for go. Supports .srt, .ttml, and .ass.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/adrg/libvlc-go">libvlc-go</a></td>
<td>Go bindings for libvlc 2.X/3.X/4.X (used by the VLC media player).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/korandiz/v4l">v4l</a></td>
<td>Video capture library for Linux, written in Go.</td>
</tr>
</tbody>
</table>

<h2>Web Frameworks</h2>

<p><em>Full stack web frameworks.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://aahframework.org">aah</a></td>
<td>Scalable, performant, rapid development Web framework for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aerogo/aero">Aero</a></td>
<td>High-performance web framework for Go, reaches top scores in Lighthouse.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aofei/air">Air</a></td>
<td>An ideally refined web framework for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nsheremet/banjo">Banjo</a></td>
<td>Very simple and fast web framework for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/astaxie/beego">Beego</a></td>
<td>beego is an open-source, high-performance web framework for the Go programming language.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://gobuffalo.io">Buffalo</a></td>
<td>Bringing the productivity of Rails to Go!</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/labstack/echo">Echo</a></td>
<td>High performance, minimalist Go web framework.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/zpatrick/fireball">Fireball</a></td>
<td>More &ldquo;natural&rdquo; feeling web framework.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-gem/gem">Gem</a></td>
<td>Simple and fast web framework, friendly to REST API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gin-gonic/gin">Gin</a></td>
<td>Gin is a web framework written in Go! It features a martini-like API with much better performance, up to 40 times faster. If you need performance and good productivity.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/NYTimes/gizmo">Gizmo</a></td>
<td>Microservice toolkit used by the New York Times.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ant0ine/go-json-rest">go-json-rest</a></td>
<td>Quick and easy way to setup a RESTful JSON API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ungerik/go-rest">go-rest</a></td>
<td>Small and evil REST framework for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/raphael/goa">goa</a></td>
<td>Framework for developing microservices based on the design of Ruby&rsquo;s Praxis.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fulldump/golax">Golax</a></td>
<td>A non Sinatra fast HTTP framework with support for Google custom methods, deep interceptors, recursion and more.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dinever/golf">Golf</a></td>
<td>Golf is a fast, simple and lightweight micro-web framework for Go. It comes with powerful features and has no dependencies other than the Go Standard Library.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rainycape/gondola">Gondola</a></td>
<td>The web framework for writing faster sites, faster.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mustafaakin/gongular">gongular</a></td>
<td>Fast Go web framework with input mapping/validation and (DI) Dependency Injection.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hidevopsio/hiboot">hiboot</a></td>
<td>hiboot is a high performance web application framework with auto configuration and dependency injection support.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-macaron/macaron">Macaron</a></td>
<td>Macaron is a high productive and modular design web framework in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/paulbellamy/mango">mango</a></td>
<td>Mango is a modular web-application framework for Go, inspired by Rack, and PEP333.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/claygod/microservice">Microservice</a></td>
<td>The framework for the creation of microservices, written in Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ivpusic/neo">neo</a></td>
<td>Neo is minimal and fast Go Web Framework with extremely simple API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-nio/nio">nio</a></td>
<td>Modern, minimal and productive Go HTTP framework.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/resoursea/api">Resoursea</a></td>
<td>REST framework for quickly writing resource based services.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://rest-layer.io">REST Layer</a></td>
<td>Framework to build REST/GraphQL API on top of databases with mostly configuration over code.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/revel/revel">Revel</a></td>
<td>High-productivity web framework for the Go language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/goanywhere/rex">rex</a></td>
<td>Rex is a library for modular development built upon gorilla/mux, fully compatible with <code>net/http</code>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jaybill/sawsij">sawsij</a></td>
<td>lightweight, open-source web framework for building high-performance, data-driven web applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/lunny/tango">tango</a></td>
<td>Micro &amp; pluggable web framework for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rcrowley/go-tigertonic">tigertonic</a></td>
<td>Go framework for building JSON web services inspired by Dropwizard.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pilu/traffic">traffic</a></td>
<td>Sinatra inspired regexp/pattern mux and web framework for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/uadmin/uadmin">uAdmin</a></td>
<td>Fully featured web framework for Golang, inspired by Django.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gernest/utron">utron</a></td>
<td>Lightweight MVC framework for Go(Golang).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/aisk/vox">vox</a></td>
<td>A golang web framework for humans, inspired by Koa heavily.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bnkamalesh/webgo">WebGo</a></td>
<td>A micro-framework to build web apps; with handler chaining, middleware and context injection. With standard library compliant HTTP handlers(i.e. http.HandlerFunc).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yarf-framework/yarf">YARF</a></td>
<td>Fast micro-framework designed to build REST APIs and web services in a fast and simple way.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cosiner/zerver">Zerver</a></td>
<td>Zerver is an expressive, modular, feature completed RESTful framework.</td>
</tr>
</tbody>
</table>

<h3>Middlewares</h3>

<h4>Actual middlewares</h4>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/posener/client-timing">client-timing</a></td>
<td>An HTTP client for Server-Timing header.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rs/cors">CORS</a></td>
<td>Easily add CORS capabilities to your API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rs/formjson">formjson</a></td>
<td>Transparently handle JSON input as a standard form POST.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mitchellh/go-server-timing">go-server-timing</a></td>
<td>Add/parse Server-Timing header.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ulule/limiter">Limiter</a></td>
<td>Dead simple rate limit middleware for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/philippgille/ln-paywall">ln-paywall</a></td>
<td>Go middleware for monetizing APIs on a per-request basis with the Lightning Network (Bitcoin)</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/didip/tollbooth">Tollbooth</a></td>
<td>Rate limit HTTP request handler.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sebest/xff">XFF</a></td>
<td>Handle <code>X-Forwarded-For</code> header and friends.</td>
</tr>
</tbody>
</table>

<h4>Libraries for creating HTTP middlewares</h4>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/justinas/alice">alice</a></td>
<td>Painless middleware chaining for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/codemodus/catena">catena</a></td>
<td>http.Handler wrapper catenation (same API as &ldquo;chain&rdquo;).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/codemodus/chain">chain</a></td>
<td>Handler wrapper chaining with scoped data (net/context-based &ldquo;middleware&rdquo;).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-on/wrap">go-wrap</a></td>
<td>Small middlewares package for net/http.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/alioygur/gores">gores</a></td>
<td>Go package that handles HTML, JSON, XML and etc. responses. Useful for RESTful APIs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/carbocation/interpose">interpose</a></td>
<td>Minimalist net/http middleware for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/stephens2424/muxchain">muxchain</a></td>
<td>Lightweight middleware for net/http.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/urfave/negroni">negroni</a></td>
<td>Idiomatic HTTP middleware for Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/unrolled/render">render</a></td>
<td>Go package for easily rendering JSON, XML, and HTML template responses.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/thedevsaddam/renderer">renderer</a></td>
<td>Simple, lightweight and faster response (JSON, JSONP, XML, YAML, HTML, File) rendering package for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/InVisionApp/rye">rye</a></td>
<td>Tiny Go middleware library (with canned Middlewares) that supports JWT, CORS, Statsd, and Go 1.7 context.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/thoas/stats">stats</a></td>
<td>Go middleware that stores various information about your web application.</td>
</tr>
</tbody>
</table>

<h3>Routers</h3>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gernest/alien">alien</a></td>
<td>Lightweight and fast http router from outer space.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-zoo/bone">Bone</a></td>
<td>Lightning Fast HTTP Multiplexer.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/claygod/Bxog">Bxog</a></td>
<td>Simple and fast HTTP router for Go. It works with routes of varying difficulty, length and nesting. And he knows how to create a URL from the received parameters.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-chi/chi">chi</a></td>
<td>Small, fast and expressive HTTP router built on net/context.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/buaazp/fasthttprouter">fasthttprouter</a></td>
<td>High performance router forked from <code>httprouter</code>. The first router fit for <code>fasthttp</code>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/razonyang/fastrouter">FastRouter</a></td>
<td>a fast, flexible HTTP router written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gocraft/web">gocraft/web</a></td>
<td>Mux and middleware package in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/goji/goji">Goji</a></td>
<td>Goji is a minimalistic and flexible HTTP request multiplexer with support for <code>net/context</code>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/vardius/gorouter">GoRouter</a></td>
<td>GoRouter is a Server/API micro framwework, HTTP request router, multiplexer, mux that provides request router with middleware supporting <code>net/context</code>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gowww/router">gowww/router</a></td>
<td>Lightning fast HTTP router fully compatible with the net/http.Handler interface.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/julienschmidt/httprouter">httprouter</a></td>
<td>High performance router. Use this and the standard http handlers to form a very high performance web framework.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dimfeld/httptreemux">httptreemux</a></td>
<td>High-speed, flexible tree-based HTTP router for Go. Inspiration from httprouter.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-playground/lars">lars</a></td>
<td>Is a lightweight, fast and extensible zero allocation HTTP router for Go used to create customizable frameworks.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gorilla/mux">mux</a></td>
<td>Powerful URL router and dispatcher for golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-ozzo/ozzo-routing">ozzo-routing</a></td>
<td>An extremely fast Go (golang) HTTP router that supports regular expression route matching. Comes with full support for building RESTful APIs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-playground/pure">pure</a></td>
<td>Is a lightweight HTTP router that sticks to the std &ldquo;net/http&rdquo; implementation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/VividCortex/siesta">Siesta</a></td>
<td>Composable framework to write middleware and handlers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/husobee/vestigo">vestigo</a></td>
<td>Performant, stand-alone, HTTP compliant URL Router for go web applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nbari/violetear">violetear</a></td>
<td>Go HTTP router.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rs/xmux">xmux</a></td>
<td>High performance muxer based on <code>httprouter</code> with <code>net/context</code> support.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xujiajun/gorouter">xujiajun/gorouter</a></td>
<td>A simple and fast HTTP router for Go.</td>
</tr>
</tbody>
</table>

<h2>Windows</h2>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gonutz/d3d9">d3d9</a></td>
<td>Go bindings for Direct3D9.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-ole/go-ole">go-ole</a></td>
<td>Win32 OLE implementation for golang.</td>
</tr>
</tbody>
</table>

<h2>XML</h2>

<p><em>Libraries and tools for manipulating XML.</em></p>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xml-comp/xml-comp">XML-Comp</a></td>
<td>Simple command line XML comparer that generates diffs of folders, files and tags.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shabbyrobe/xmlwriter">xmlwriter</a></td>
<td>Procedural XML generation API based on libxml2&rsquo;s xmlwriter module.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/antchfx/xpath">xpath</a></td>
<td>XPath package for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/antchfx/xquery">xquery</a></td>
<td>XQuery lets you extract data from HTML/XML documents using XPath expression.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sbabiv/xml2map">xml2map</a></td>
<td>XML to MAP converter written Golang.</td>
</tr>
</tbody>
</table>

<h1>Tools</h1>

<p><em>Go software and plugins.</em></p>

<h2>Code Analysis</h2>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bradleyfalzon/apicompat">apicompat</a></td>
<td>Checks recent changes to a Go project for backwards incompatible changes.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mibk/dupl">dupl</a></td>
<td>Tool for code clone detection.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kisielk/errcheck">errcheck</a></td>
<td>Errcheck is a program for checking for unchecked errors in Go programs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/davecheney/gcvis">gcvis</a></td>
<td>Visualise Go program GC trace data in real time.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/alecthomas/gometalinter">Go Metalinter</a></td>
<td>Metalinter is a tool to automatically apply all static analysis tool and report their output in normalized form.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/qiniu/checkstyle">go-checkstyle</a></td>
<td>checkstyle is a style check tool like java checkstyle. This tool inspired by java checkstyle, golint. The style refered to some points in Go Code Review Comments.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/roblaszczak/go-cleanarch">go-cleanarch</a></td>
<td>go-cleanarch was created to validate Clean Architecture rules, like a The Dependency Rule and interaction between packages in your Go projects.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-critic/go-critic">go-critic</a></td>
<td>source code linter that brings checks that are currently not implemented in other linters.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/firstrow/go-outdated">go-outdated</a></td>
<td>Console application that displays outdated packages.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yuroyoro/goast-viewer">goast-viewer</a></td>
<td>Web based Golang AST visualizer.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://gocover.io/">GoCover.io</a></td>
<td>GoCover.io offers the code coverage of any golang package as a service.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://godoc.org/golang.org/x/tools/cmd/goimports">goimports</a></td>
<td>Tool to fix (add, remove) your Go imports automatically.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://golangci.com/">GolangCI</a></td>
<td>GolangCI is an automated Golang code review service for GitHub pull requests. Service is open source and it&rsquo;s free for open source projects.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/golang/lint">GoLint</a></td>
<td>Golint is a linter for Go source code.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://go-lint.appspot.com/">Golint online</a></td>
<td>Lints online Go source files on GitHub, Bitbucket and Google Project Hosting using the golint package.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://sourcegraph.com/github.com/sqs/goreturns">goreturns</a></td>
<td>Adds zero-value return statements to match the func return types.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dominikh/go-tools/tree/master/cmd/gosimple">gosimple</a></td>
<td>gosimple is a linter for Go source code that specialises on simplifying code.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shurcooL/gostatus">gostatus</a></td>
<td>Command line tool, shows the status of repositories that contain Go packages.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/surullabs/lint">lint</a></td>
<td>Run linters as part of go test.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/z7zmey/php-parser">php-parser</a></td>
<td>A Parser for PHP written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dominikh/go-tools/tree/master/cmd/staticcheck">staticcheck</a></td>
<td>staticcheck is <code>go vet</code> on steroids, applying a ton of static analysis checks you might be used to from tools like ReSharper for C#.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/verygoodsoftwarenotvirus/tarp">tarp</a></td>
<td>tarp finds functions and methods without direct unit tests in Go source code.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mdempsky/unconvert">unconvert</a></td>
<td>Remove unnecessary type conversions from Go source.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dominikh/go-tools/tree/master/cmd/unused">unused</a></td>
<td>unused checks Go code for unused constants, variables, functions and types.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mccoyst/validate">validate</a></td>
<td>Automatically validates struct fields with tags.</td>
</tr>
</tbody>
</table>

<h2>Editor Plugins</h2>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://plugins.jetbrains.com/plugin/9568-go">Go plugin for JetBrains IDEs</a></td>
<td>Go plugin for JetBrains IDEs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/theia-ide/go-language-server">go-language-server</a></td>
<td>A wrapper to turn the VSCode go extension into a language server supporting the language-server-protocol.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dominikh/go-mode.el">go-mode</a></td>
<td>Go mode for GNU/Emacs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/joefitzgerald/go-plus">go-plus</a></td>
<td>Go (Golang) Package For Atom That Adds Autocomplete, Formatting, Syntax Checking, Linting and Vetting.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/nsf/gocode">gocode</a></td>
<td>Autocompletion daemon for the Go programming language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/DisposaBoy/GoSublime">GoSublime</a></td>
<td>Golang plugin collection for the text editor SublimeText 3 providing code completion and other IDE-like features.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hexdigest/gounit-vim">gounit-vim</a></td>
<td>Vim plugin for generating Go tests based on the function&rsquo;s or method&rsquo;s signature.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/theia-ide/theia-go-extension">theia-go-extension</a></td>
<td>Go language support for the Theia IDE.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/velour/velour">velour</a></td>
<td>IRC client for the acme editor.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rjohnsondev/vim-compiler-go">vim-compiler-go</a></td>
<td>Vim plugin to highlight syntax errors on save.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fatih/vim-go">vim-go</a></td>
<td>Go development plugin for Vim.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Microsoft/vscode-go">vscode-go</a></td>
<td>Extension for Visual Studio Code (VS Code) which provides support for the Go language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/eaburns/Watch">Watch</a></td>
<td>Runs a command in an acme win on file changes.</td>
</tr>
</tbody>
</table>

<h2>Go Generate Tools</h2>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/usk81/generic">generic</a></td>
<td>flexible data type for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cheekybits/genny">genny</a></td>
<td>Elegant generics for Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Parquery/gocontracts">gocontracts</a></td>
<td>brings design-by-contract to Go by synchronizing the code with the documentation.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="http://github.com/bouk/gonerics">gonerics</a></td>
<td>Idiomatic Generics in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cweill/gotests">gotests</a></td>
<td>Generate Go tests from your source code.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hexdigest/gounit">gounit</a></td>
<td>Generate Go tests using your own templates.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/opennota/re2dfa">re2dfa</a></td>
<td>Transform regular expressions into finite state machines and output Go source code.</td>
</tr>
</tbody>
</table>

<h2>Go Tools</h2>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/songgao/colorgo">colorgo</a></td>
<td>Wrapper around <code>go</code> command for colorized <code>go build</code> output.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/KyleBanks/depth">depth</a></td>
<td>Visualize dependency trees of any package by analyzing imports.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://getgb.io/">gb</a></td>
<td>An easy to use project based build tool for the Go programming language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/axelspringer/generator-go-lang">generator-go-lang</a></td>
<td>A <a href="http://yeoman.io">Yeoman</a> generator to get new Go projects started.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/TrueFurby/go-callvis">go-callvis</a></td>
<td>Visualize call graph of your Go program using dot format.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/skelterjohn/go-pkg-complete">go-pkg-complete</a></td>
<td>Bash completion for go and wgo.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-swagger/go-swagger">go-swagger</a></td>
<td>Swagger 2.0 implementation for go. Swagger is a simple yet powerful representation of your RESTful API.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/OctoLinker/browser-extension">OctoLinker</a></td>
<td>Navigate through go files efficiently with the OctoLinker browser extension for GitHub.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kyoh86/richgo">richgo</a></td>
<td>Enrich <code>go test</code> outputs with text decorations.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/galeone/rts">rts</a></td>
<td>RTS: response to struct. Generates Go structs from server responses.</td>
</tr>
</tbody>
</table>

<h2>Software Packages</h2>

<p><em>Software written in Go.</em></p>

<h3>DevOps Tools</h3>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/smira/aptly">aptly</a></td>
<td>aptly is a Debian repository management tool.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xuri/aurora">aurora</a></td>
<td>Cross-platform web-based Beanstalkd queue server console.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/soniah/awsenv">awsenv</a></td>
<td>Small binary that loads Amazon (AWS) environment variables for a profile.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/eleme/banshee">Banshee</a></td>
<td>Anomalies detection system for periodic metrics.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dave/blast">Blast</a></td>
<td>A simple tool for API load testing and batch jobs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/codesenberg/bombardier">bombardier</a></td>
<td>Fast cross-platform HTTP benchmarking tool.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bosun-monitor/bosun">bosun</a></td>
<td>Time Series Alerting Framework.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/centerorbit/depcharge">DepCharge</a></td>
<td>Helps orchestrating the execution of commands across the many dependencies in larger projects.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/liudng/dogo">dogo</a></td>
<td>Monitoring changes in the source file and automatically compile and run (restart).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/appleboy/drone-jenkins">drone-jenkins</a></td>
<td>Trigger downstream Jenkins jobs using a binary, docker or Drone CI.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/appleboy/drone-scp">drone-scp</a></td>
<td>Copy files and artifacts via SSH using a binary, docker or Drone CI.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/chrismckenzie/dropship">Dropship</a></td>
<td>Tool for deploying code via cdn.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/appleboy/easyssh-proxy">easyssh-proxy</a></td>
<td>Golang package for easy remote execution through SSH and SCP downloading via <code>ProxyCommand</code>.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mkchoi212/fac">fac</a></td>
<td>Command-line user interface to fix git merge conflicts.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gaia-pipeline/gaia">gaia</a></td>
<td>Build powerful pipelines in any programming language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-gitea/gitea">Gitea</a></td>
<td>Fork of Gogs, entirely community driven.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://git.jonasfranz.software/JonasFranzDEV/gitea-github-migrator">gitea-github-migrator</a></td>
<td>Migrate all your GitHub repositories, issues, milestones and labels to your Gitea instance.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/go-furnace/go-furnace">go-furnace</a></td>
<td>Hosting solution written in Go. Deploy your Application with ease on AWS, GCP or DigitalOcean.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sanbornm/go-selfupdate">go-selfupdate</a></td>
<td>Enable your Go applications to self update.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cryptojuice/gobrew">gobrew</a></td>
<td>gobrew lets you easily switch between multiple versions of go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sirnewton01/godbg">godbg</a></td>
<td>Web-based gdb front-end application.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://gogs.io/">Gogs</a></td>
<td>A Self Hosted Git Service in the Go Programming Language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/inconshreveable/gonative">gonative</a></td>
<td>Tool which creates a build of Go that can cross compile to all platforms while still using the Cgo-enabled versions of the stdlib packages.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ahmetalpbalkan/govvv">govvv</a></td>
<td>“go build” wrapper to easily add version information into Go binaries.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mitchellh/gox">gox</a></td>
<td>Dead simple, no frills Go cross compile tool.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/laher/goxc">goxc</a></td>
<td>build tool for Go, with a focus on cross-compiling and packaging.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yaronsumel/grapes">grapes</a></td>
<td>Lightweight tool designed to distribute commands over ssh with ease.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/moovweb/gvm">GVM</a></td>
<td>GVM provides an interface to manage Go versions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rakyll/hey">Hey</a></td>
<td>Hey is a tiny program that sends some load to a web application.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ajvb/kala">kala</a></td>
<td>Simplistic, modern, and performant job scheduler.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/cswank/kcli">kcli</a></td>
<td>Command line tool for inspecting kafka topics/partitions/messages.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kubernetes/kubernetes">kubernetes</a></td>
<td>Container Cluster Manager from Google.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ivanilves/lstags">lstags</a></td>
<td>Tool and API to sync Docker images across different registries.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/timdp/lwc">lwc</a></td>
<td>A live-updating version of the UNIX wc command.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/xwjdsh/manssh">manssh</a></td>
<td>manssh is a command line tool for managing your ssh alias config easily.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/moby/moby">Moby</a></td>
<td>Collaborative project for the container ecosystem to assemble container-based systems.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/emicklei/mora">Mora</a></td>
<td>REST server for accessing MongoDB documents and meta data.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ostrost/ostent">ostent</a></td>
<td>collects and displays system metrics and optionally relays to Graphite and/or InfluxDB.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mitchellh/packer">Packer</a></td>
<td>Packer is a tool for creating identical machine images for multiple platforms from a single source configuration.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bengadbois/pewpew">Pewpew</a></td>
<td>Flexible HTTP command line stress tester.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/alouche/rodent">Rodent</a></td>
<td>Rodent helps you manage Go versions, projects and track dependencies.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rlmcpherson/s3gof3r">s3gof3r</a></td>
<td>Small utility/library optimized for high speed transfer of large objects into and out of Amazon S3.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/scaleway/scaleway-cli">Scaleway-cli</a></td>
<td>Manage BareMetal Servers from Command Line (as easily as with Docker).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ChristopherRabotin/sg">sg</a></td>
<td>Benchmarks a set of HTTP endpoints (like ab), with possibility to use the response code and data between each call for specific server stress based on its previous response.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/TimothyYe/skm">skm</a></td>
<td>SKM is a simple and powerful SSH Keys Manager, it helps you to manage your multiple SSH keys easily!</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sanathp/statusok">StatusOK</a></td>
<td>Monitor your Website and REST APIs.Get Notified through Slack, E-mail when your server is down or response time is more than expected.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/containous/traefik">traefik</a></td>
<td>Reverse proxy and load balancer with support for multiple backends.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tsenart/vegeta">Vegeta</a></td>
<td>HTTP load testing tool and library. It&rsquo;s over 9000!</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/adnanh/webhook">webhook</a></td>
<td>Tool which allows user to create HTTP endpoints (hooks) that execute commands on the server.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://wide.b3log.org/login">Wide</a></td>
<td>Web-based IDE for Teams using Golang.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/masterzen/winrm-cli">winrm-cli</a></td>
<td>Cli tool to remotely execute commands on Windows machines.</td>
</tr>
</tbody>
</table>

<h3>Other Software</h3>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/crufter/borg">borg</a></td>
<td>Terminal based search engine for bash snippets.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tejo/boxed">boxed</a></td>
<td>Dropbox based blog engine.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rafael-santiago/cherry">Cherry</a></td>
<td>Tiny webchat server in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gocircuit/circuit">Circuit</a></td>
<td>Circuit is a programmable platform-as-a-service (PaaS) and/or Infrastructure-as-a-Service (IaaS), for management, discovery, synchronization and orchestration of services and hosts comprising cloud applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tylertreat/Comcast">Comcast</a></td>
<td>Simulate bad network connections.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/kelseyhightower/confd">confd</a></td>
<td>Manage local application configuration files using templates and data from etcd or consul.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/skibish/ddns">DDNS</a></td>
<td>Personal DDNS client with Digital Ocean Networking DNS as backend.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://www.docker.com/">Docker</a></td>
<td>Open platform for distributed applications for developers and sysadmins.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/documize/community">Documize</a></td>
<td>Modern wiki software that integrates data from SaaS tools.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/odeke-em/drive">drive</a></td>
<td>Google Drive client for the commandline.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gilbertchen/duplicacy">Duplicacy</a></td>
<td>A cross-platform network and cloud backup tool based on the idea of lock-free deduplication.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shurcooL/Go-Package-Store">Go Package Store</a></td>
<td>App that displays updates for the Go packages in your GOPATH.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/Humpheh/goboy">GoBoy</a></td>
<td>Nintendo Game Boy Color emulator written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/goccmack/gocc">gocc</a></td>
<td>Gocc is a compiler kit for Go written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/timothyye/godns">GoDNS</a></td>
<td>A dynamic DNS client tool, supports DNSPod &amp; HE.net, written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/diankong/GoDocTooltip">GoDocTooltip</a></td>
<td>Chrome extension for Go Doc sites, which shows function description as tooltip at function list.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://jetbrains.com/go">GoLand</a></td>
<td>Full featured cross-platform Go IDE.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/buger/gor">Gor</a></td>
<td>Http traffic replication tool, for replaying traffic from production to stage/dev environments in real-time.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://gohugo.io/">hugo</a></td>
<td>Fast and Modern Static Website Engine.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/thestrukture/ide">ide</a></td>
<td>Browser accessible IDE. Designed for Go with Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dimiro1/ipe">ipe</a></td>
<td>Open source Pusher server implementation compatible with Pusher client libraries written in GO.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/assafmo/joincap">joincap</a></td>
<td>Command-line utility for merging multiple pcap files together.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://jujucharms.com/">Juju</a></td>
<td>Cloud-agnostic service deployment and orchestration - supports EC2, Azure, Openstack, MAAS and more.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jeffail/leaps">Leaps</a></td>
<td>Pair programming service using Operational Transforms.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/yunabe/lgo">lgo</a></td>
<td>Interactive Go programming with Jupyter. It supports code completion, code inspection and 100% Go compatibility.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://limetext.org/">limetext</a></td>
<td>Lime Text is a powerful and elegant text editor primarily developed in Go that aims to be a Free and open-source software successor to Sublime Text.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/visualfc/liteide">LiteIDE</a></td>
<td>LiteIDE is a simple, open source, cross-platform Go IDE.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/quii/mockingjay-server">mockingjay</a></td>
<td>Fake HTTP servers and consumer driven contracts from one configuration file. You can also make the server randomly misbehave to help do more realistic performance tests.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mehrdadrad/mylg">myLG</a></td>
<td>Command Line Network Diagnostic tool written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/unix4fun/naclpipe">naclpipe</a></td>
<td>Simple NaCL EC25519 based crypto pipe tool written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fogleman/nes">nes</a></td>
<td>Nintendo Entertainment System (NES) emulator written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/noraesae/orange-cat">orange-cat</a></td>
<td>Markdown previewer written in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/gulien/orbit">Orbit</a></td>
<td>A simple tool for running commands and generating files from templates.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pointlander/peg">peg</a></td>
<td>Peg, Parsing Expression Grammar, is an implementation of a Packrat parser generator.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/b3log/pipe">Pipe</a></td>
<td>A small and beautiful blogging platform.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/restic/restic">restic</a></td>
<td>De-duplicating backup program.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/coreos/rkt">rkt</a></td>
<td>App Container runtime that integrates with init systems, is compatible with other container formats like Docker, and supports alternative execution engines like KVM.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/boyter/scc">scc</a></td>
<td>Sloc Cloc and Code, a very fast accurate code counter with complexity calculations and COCOMO estimates.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/chrislusf/seaweedfs">Seaweed File System</a></td>
<td>Fast, Simple and Scalable Distributed File System with O(1) disk seek.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/msoap/shell2http">shell2http</a></td>
<td>Executing shell commands via http server (for prototyping or remote control).</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/intelsdi-x/snap">snap</a></td>
<td>Powerful telemetry framework.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/lucasgomide/snitch">Snitch</a></td>
<td>Simple way to notify your team and many tools when someone has deployed any application via Tsuru.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/pressly/sup">Stack Up</a></td>
<td>Stack Up, a super simple deployment tool - just Unix - think of it like &lsquo;make&rsquo; for a network of servers.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://syncthing.net/">syncthing</a></td>
<td>Open, decentralized file synchronization tool and protocol.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/crazcalm/term-quiz">term-quiz</a></td>
<td>Quizzes for your terminal.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shopify/toxiproxy">toxiproxy</a></td>
<td>Proxy to simulate network and system conditions for automated tests.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://tsuru.io/">tsuru</a></td>
<td>Extensible and open source Platform as a Service software.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/VerizonDigital/vflow">vFlow</a></td>
<td>High-performance, scalable and reliable IPFIX, sFlow and Netflow collector.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/wellington/wellington">wellington</a></td>
<td>Sass project management tool, extends the language with sprite functions (like Compass).</td>
</tr>
</tbody>
</table>

<h1>Resources</h1>

<p><em>Where to discover new Go libraries.</em></p>

<h2>Benchmarks</h2>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/davecheney/autobench">autobench</a></td>
<td>Framework to compare the performance between different Go versions.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mrLSD/go-benchmark-app">go-benchmark-app</a></td>
<td>Powerful HTTP-benchmark tool mixed with Аb, Wrk, Siege tools. Gathering statistics and various parameters for benchmarks and comparison results.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tylertreat/go-benchmarks">go-benchmarks</a></td>
<td>Few miscellaneous Go microbenchmarks. Compare some language features to alternative approaches.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/julienschmidt/go-http-routing-benchmark">go-http-routing-benchmark</a></td>
<td>Go HTTP request router benchmark and comparison.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/hgfischer/go-type-assertion-benchmark">go-type-assertion-benchmark</a></td>
<td>Naive performance test of two ways to do type assertion in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/smallnest/go-web-framework-benchmark">go-web-framework-benchmark</a></td>
<td>Go web framework benchmark.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/alecthomas/go_serialization_benchmarks">go_serialization_benchmarks</a></td>
<td>Benchmarks of Go serialization methods.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/PuerkitoBio/gocostmodel">gocostmodel</a></td>
<td>Benchmarks of common basic operations for the Go language.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/amscanne/golang-micro-benchmarks">golang-micro-benchmarks</a></td>
<td>Tiny collection of Go micro benchmarks. The intent is to compare some language features to others.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tyler-smith/golang-sql-benchmark">golang-sql-benchmark</a></td>
<td>Collection of benchmarks for popular Go database/SQL utilities.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/feyeleanor/GoSpeed">gospeed</a></td>
<td>Go micro-benchmarks for calculating the speed of language constructs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/jimrobinson/kvbench">kvbench</a></td>
<td>Key/Value database benchmark.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/atemerev/skynet">skynet</a></td>
<td>Skynet 1M threads microbenchmark.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/fawick/speedtest-resize">speedtest-resize</a></td>
<td>Compare various Image resize algorithms for the Go language.</td>
</tr>
</tbody>
</table>

<h2>Conferences</h2>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://www.capitalgolang.com">Capital Go</a></td>
<td>Washington, D.C., USA.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://www.dotgo.eu">dotGo</a></td>
<td>Paris, France.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://gocon.connpass.com/">GoCon</a></td>
<td>Tokyo, Japan.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.godays.io/">GoDays</a></td>
<td>Berlin, Germany.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://golab.io/">GoLab</a></td>
<td>Florence, Italy.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://golanguk.com/">GolangUK</a></td>
<td>London, UK.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://gopherchina.org">GopherChina</a></td>
<td>Shanghai, China.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://www.gophercon.com/">GopherCon</a></td>
<td>Denver, USA.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://gopherconbr.org">GopherCon Brazil</a></td>
<td>Florianópolis, BR.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://gophercon.is/">GopherCon Europe</a></td>
<td>Reykjavik, Iceland.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.gophercon.in/">GopherCon India</a></td>
<td>Pune, India.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.gophercon.org.il/">GopherCon Israel</a></td>
<td>Tel Aviv, Israel.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.gophercon-russia.ru">GopherCon Russia</a></td>
<td>Moscow, Russia.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://gophercon.sg">GopherCon Singapore</a></td>
<td>Mapletree Business City, Singapore.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://gothamgo.com/">GothamGo</a></td>
<td>New York City, USA.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://goway.io/">GoWayFest</a></td>
<td>Minsk, Belarus.</td>
</tr>
</tbody>
</table>

<h2>E-Books</h2>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://leanpub.com/GoNotebook/read">A Go Developer&rsquo;s Notebook</a></td>
<td><a href="https://leanpub.com/GoNotebook/read">A Go Developer&rsquo;s Notebook</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://www.golang-book.com/">An Introduction to Programming in Go</a></td>
<td><a href="http://www.golang-book.com/">An Introduction to Programming in Go</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.gitbook.com/book/astaxie/build-web-application-with-golang/details">Build Web Application with Golang</a></td>
<td><a href="https://www.gitbook.com/book/astaxie/build-web-application-with-golang/details">Build Web Application with Golang</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.gitbook.com/book/codegangsta/building-web-apps-with-go/details">Building Web Apps With Go</a></td>
<td><a href="https://www.gitbook.com/book/codegangsta/building-web-apps-with-go/details">Building Web Apps With Go</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://go101.org">Go 101</a></td>
<td>A book focusing on Go syntax/semantics and all kinds of details.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://golangbootcamp.com">Go Bootcamp</a></td>
<td><a href="http://golangbootcamp.com">Go Bootcamp</a></td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/dariubs/GoBooks">GoBooks</a></td>
<td>A curated list of Go books.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.miek.nl/downloads/Go/Learning-Go-latest.pdf">Learning Go</a></td>
<td><a href="https://www.miek.nl/downloads/Go/Learning-Go-latest.pdf">Learning Go</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://jan.newmarch.name/go/">Network Programming With Go</a></td>
<td><a href="https://jan.newmarch.name/go/">Network Programming With Go</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://www.gopl.io/">The Go Programming Language</a></td>
<td><a href="http://www.gopl.io/">The Go Programming Language</a></td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/thewhitetulip/web-dev-golang-anti-textbook/">Web Application with Go the Anti-Textbook</a></td>
<td><a href="https://github.com/thewhitetulip/web-dev-golang-anti-textbook/">Web Application with Go the Anti-Textbook</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://compilerbook.com">Writing A Compiler In Go</a></td>
<td><a href="https://compilerbook.com">Writing A Compiler In Go</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://interpreterbook.com">Writing An Interpreter In Go</a></td>
<td><a href="https://interpreterbook.com">Writing An Interpreter In Go</a></td>
</tr>
</tbody>
</table>

<h2>Gophers</h2>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/keygx/Go-gopher-Vector">Go-gopher-Vector</a></td>
<td>Go gopher Vector Data [.ai, .svg].</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/GolangUA/gopher-logos">gopher-logos</a></td>
<td>adorable gopher logos.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/tenntenn/gopher-stickers">gopher-stickers</a></td>
<td><a href="https://github.com/tenntenn/gopher-stickers">gopher-stickers</a></td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/golang-samples/gopher-vector">gopher-vector</a></td>
<td><a href="https://github.com/golang-samples/gopher-vector">gopher-vector</a></td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/shalakhin/gophericons">gophericons</a></td>
<td><a href="https://github.com/shalakhin/gophericons">gophericons</a></td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/matryer/gopherize.me">gopherize.me</a></td>
<td>Gopherize yourself.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ashleymcnamara/gophers">gophers</a></td>
<td>Gopher artworks by Ashley McNamara.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/egonelbre/gophers">gophers</a></td>
<td>Free gophers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/rogeralsing/gophers">gophers</a></td>
<td>random gopher graphics.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/sillecelik/go-gopher">gophers</a></td>
<td>Gopher amigurumi toy pattern.</td>
</tr>
</tbody>
</table>

<h2>Meetups</h2>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/golanguagenewyork/">Go Language NYC</a></td>
<td><a href="https://www.meetup.com/golanguagenewyork/">Go Language NYC</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Go-London-User-Group/">Go London User Group</a></td>
<td><a href="https://www.meetup.com/Go-London-User-Group/">Go London User Group</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/go-toronto/">Go Toronto</a></td>
<td><a href="https://www.meetup.com/go-toronto/">Go Toronto</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Go-Users-Group-Atlanta/">Go User Group Atlanta</a></td>
<td><a href="https://www.meetup.com/Go-Users-Group-Atlanta/">Go User Group Atlanta</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/gobridge/">GoBridge, San Francisco, CA</a></td>
<td><a href="https://www.meetup.com/gobridge/">GoBridge, San Francisco, CA</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/GoJakarta/">GoJakarta</a></td>
<td><a href="https://www.meetup.com/GoJakarta/">GoJakarta</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/golang-amsterdam/">Golang Amsterdam</a></td>
<td><a href="https://www.meetup.com/golang-amsterdam/">Golang Amsterdam</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Golang-Argentina/">Golang Argentina</a></td>
<td><a href="https://www.meetup.com/Golang-Argentina/">Golang Argentina</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/BaltimoreGolang/">Golang Baltimore, MD</a></td>
<td><a href="https://www.meetup.com/BaltimoreGolang/">Golang Baltimore, MD</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Golang-Bangalore/">Golang Bangalore</a></td>
<td><a href="https://www.meetup.com/Golang-Bangalore/">Golang Bangalore</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/go-belo-horizonte/">Golang Belo Horizonte - Brazil</a></td>
<td>Brazil](<a href="https://www.meetup.com/go-belo-horizonte/">https://www.meetup.com/go-belo-horizonte/</a>)</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/bostongo/">Golang Boston</a></td>
<td><a href="https://www.meetup.com/bostongo/">Golang Boston</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Golang-Bulgaria/">Golang Bulgaria</a></td>
<td><a href="https://www.meetup.com/Golang-Bulgaria/">Golang Bulgaria</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Cardiff-Go-Meetup/">Golang Cardiff, UK</a></td>
<td><a href="https://www.meetup.com/Cardiff-Go-Meetup/">Golang Cardiff, UK</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Go-Cph/">Golang Copenhagen</a></td>
<td><a href="https://www.meetup.com/Go-Cph/">Golang Copenhagen</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Golang-DC/">Golang DC, Arlington, VA</a></td>
<td><a href="https://www.meetup.com/Golang-DC/">Golang DC, Arlington, VA</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/golang-dorset/">Golang Dorset, UK</a></td>
<td><a href="https://www.meetup.com/golang-dorset/">Golang Dorset, UK</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Go-User-Group-Hamburg/">Golang Hamburg - Germany</a></td>
<td>Germany](<a href="https://www.meetup.com/Go-User-Group-Hamburg/">https://www.meetup.com/Go-User-Group-Hamburg/</a>)</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Go-Israel/">Golang Israel</a></td>
<td><a href="https://www.meetup.com/Go-Israel/">Golang Israel</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Joinville-Go-Meetup/">Golang Joinville - Brazil</a></td>
<td>Brazil](<a href="https://www.meetup.com/Joinville-Go-Meetup/">https://www.meetup.com/Joinville-Go-Meetup/</a>)</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Golang-Peru/">Golang Lima - Peru</a></td>
<td>Peru](<a href="https://www.meetup.com/Golang-Peru/">https://www.meetup.com/Golang-Peru/</a>)</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Golang-Lyon/">Golang Lyon</a></td>
<td><a href="https://www.meetup.com/Golang-Lyon/">Golang Lyon</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/golang-mel/">Golang Melbourne</a></td>
<td><a href="https://www.meetup.com/golang-mel/">Golang Melbourne</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Golang-Mountain-View/">Golang Mountain View</a></td>
<td><a href="https://www.meetup.com/Golang-Mountain-View/">Golang Mountain View</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/nycgolang/">Golang New York</a></td>
<td><a href="https://www.meetup.com/nycgolang/">Golang New York</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Golang-Paris/">Golang Paris</a></td>
<td><a href="https://www.meetup.com/Golang-Paris/">Golang Paris</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Golang-Pune/">Golang Pune</a></td>
<td><a href="https://www.meetup.com/Golang-Pune/">Golang Pune</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/golangsg/">Golang Singapore</a></td>
<td><a href="https://www.meetup.com/golangsg/">Golang Singapore</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Go-Stockholm/">Golang Stockholm</a></td>
<td><a href="https://www.meetup.com/Go-Stockholm/">Golang Stockholm</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/golang-syd/">Golang Sydney, AU</a></td>
<td><a href="https://www.meetup.com/golang-syd/">Golang Sydney, AU</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/golangbr/">Golang São Paulo - Brazil</a></td>
<td>Brazil](<a href="https://www.meetup.com/golangbr/">https://www.meetup.com/golangbr/</a>)</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/golangvan/">Golang Vancouver, BC</a></td>
<td><a href="https://www.meetup.com/golangvan/">Golang Vancouver, BC</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Golang-Moscow/">Golang Москва</a></td>
<td><a href="https://www.meetup.com/Golang-Moscow/">Golang Москва</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Golang-Peter/">Golang Питер</a></td>
<td><a href="https://www.meetup.com/Golang-Peter/">Golang Питер</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Istanbul-Golang/">Istanbul Golang</a></td>
<td><a href="https://www.meetup.com/Istanbul-Golang/">Istanbul Golang</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/golang/">Seattle Go Programmers</a></td>
<td><a href="https://www.meetup.com/golang/">Seattle Go Programmers</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/uagolang/">Ukrainian Golang User Groups</a></td>
<td><a href="https://www.meetup.com/uagolang/">Ukrainian Golang User Groups</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/utahgophers/">Utah Go User Group</a></td>
<td><a href="https://www.meetup.com/utahgophers/">Utah Go User Group</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.meetup.com/Women-Who-Go/">Women Who Go - San Francisco, CA</a></td>
<td>San Francisco, CA](<a href="https://www.meetup.com/Women-Who-Go/">https://www.meetup.com/Women-Who-Go/</a>)</td>
</tr>
</tbody>
</table>
<p>*Add the group of your city/country here (send <strong>PR</strong>)*</p>

<h2>Twitter</h2>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://twitter.com/golang">@golang</a></td>
<td><a href="https://twitter.com/golang">@golang</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://twitter.com/golang_news">@golang_news</a></td>
<td><a href="https://twitter.com/golang_news">@golang_news</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://twitter.com/golangch">@golangch</a></td>
<td><a href="https://twitter.com/golangch">@golangch</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://twitter.com/golangflow">@golangflow</a></td>
<td><a href="https://twitter.com/golangflow">@golangflow</a></td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://twitter.com/golangweekly">@golangweekly</a></td>
<td><a href="https://twitter.com/golangweekly">@golangweekly</a></td>
</tr>
</tbody>
</table>

<h2>Websites</h2>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://go.libhunt.com">Awesome Go @LibHunt</a></td>
<td>Your go-to Go Toolbox.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/lukasz-madon/awesome-remote-job">Awesome Remote Job</a></td>
<td>Curated list of awesome remote jobs. A lot of them are looking for Go hackers.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/bayandin/awesome-awesomeness">awesome-awesomeness</a></td>
<td>List of other amazingly awesome lists.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.codingame.com/">CodinGame</a></td>
<td>Learn Go by solving interactive tasks using small games as practical examples.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://blog.golang.org">Go Blog</a></td>
<td>The official Go blog.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://golang-challenge.org/">Go Challenge</a></td>
<td>Learn Go by solving problems and getting feedback from Go experts.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://forum.golangbridge.org">Go Forum</a></td>
<td>Forum to discuss Go.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.goin5minutes.com/">Go In 5 Minutes</a></td>
<td>5 minute screencasts focused on getting one thing done.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/golang/go/wiki/Projects">Go Projects</a></td>
<td>List of projects on the Go community wiki.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://goreportcard.com">Go Report Card</a></td>
<td>A report card for your Go package.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/ninedraft/gocryforhelp">gocryforhelp</a></td>
<td>Collection of Go projects that needs help. Good place to start your open-source way in Go.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://godoc.org/">godoc.org</a></td>
<td>Documentation for open source Go packages.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://golangflow.io">Golang Flow</a></td>
<td>Post Updates, News, Packages and more.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://golangnews.com">Golang News</a></td>
<td>Links and news about Go programming.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mholt/golang-graphics">golang-graphics</a></td>
<td>Collection of Go images, graphics, and art.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://groups.google.com/forum/#!forum/golang-nuts">golang-nuts</a></td>
<td>Go mailing list.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://plus.google.com/communities/114112804251407510571">Google Plus Community</a></td>
<td>The Google+ community for #golang enthusiasts.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://invite.slack.golangbridge.org">Gopher Community Chat</a></td>
<td>Join Our New Slack Community For Gophers (<a href="https://blog.gopheracademy.com/gophers-slack-community/">Understand how it came</a>).</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://gowalker.org">gowalker.org</a></td>
<td>Go Project API documentation.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.youtube.com/c/justforfunc">justforfunc</a></td>
<td>Youtube channel dedicated to Go programming language tips and tricks, hosted by  Francesc Campoy <a href="https://twitter.com/francesc">@francesc</a>.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.reddit.com/r/golang">r/Golang</a></td>
<td>News about Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/trending?l=go">Trending Go repositories on GitHub today</a></td>
<td>Good place to find new Go libraries.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://tutorialedge.net/course/golang/">TutorialEdge - Golang</a></td>
<td>Golang](<a href="https://tutorialedge.net/course/golang/">https://tutorialedge.net/course/golang/</a>)</td>
</tr>
</tbody>
</table>

<h3>Tutorials</h3>

<table>
<thead>
<tr>
<th>Stars</th>
<th>Forks</th>
<th>Issues</th>
<th>Last Commit</th>
<th>Name</th>
<th>Desc</th>
</tr>
</thead>

<tbody>
<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://devs.cloudimmunity.com/gotchas-and-common-mistakes-in-go-golang/">50 Shades of Go</a></td>
<td>Traps, Gotchas, and Common Mistakes for New Golang Devs.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://tour.golang.org/">A Tour of Go</a></td>
<td>Interactive tour of Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/astaxie/build-web-application-with-golang">Build web application with Golang</a></td>
<td>Golang ebook intro how to build a web app with golang.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://semaphoreci.com/community/tutorials/building-go-web-applications-and-microservices-using-gin">Building Go Web Applications and Microservices Using Gin</a></td>
<td>Get familiar with Gin and find out how it can help you reduce boilerplate code and build a request handling pipeline.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://medium.com/@rocketlaunchr.cloud/canceling-mysql-in-go-827ed8f83b30">Canceling MySQL</a></td>
<td>How to cancel MySQL queries.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/miguelmota/ethereum-development-with-go-book">Ethereum Development with Go</a></td>
<td>A little e-book on Ethereum Development with Go.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://gameswithgo.org/">Games With Go</a></td>
<td>A video series teaching programming and game development.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://gobyexample.com/">Go By Example</a></td>
<td>Hands-on introduction to Go using annotated example programs.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/a8m/go-lang-cheat-sheet">Go Cheat Sheet</a></td>
<td>Go&rsquo;s reference card.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://go-database-sql.org/">Go database/sql tutorial</a></td>
<td>Introduction to database/sql.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://itunes.apple.com/us/app/go-playground/id1437518275?ls=1&amp;mt=8">Go Playground for iOS</a></td>
<td>Interactively edit &amp; play Go snippets on your mobile device.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://tutorialedge.net/golang/go-webassembly-tutorial/">Go WebAssembly Tutorial - Building a Simple Calculator</a></td>
<td>Building a Simple Calculator](<a href="https://tutorialedge.net/golang/go-webassembly-tutorial/">https://tutorialedge.net/golang/go-webassembly-tutorial/</a>)</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/miguelmota/golang-for-nodejs-developers">Golang for Node.js Developers</a></td>
<td>Examples of Golang compared to Node.js for learning.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://golangbot.com/learn-golang-series/">Golangbot</a></td>
<td>Tutorials to get started with programming in Go.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://hackr.io/tutorials/learn-golang">Hackr.io</a></td>
<td>Learn Go from the best online golang tutorials submitted &amp; voted by the golang programming community.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://semaphoreci.com/community/tutorials/how-to-use-godog-for-behavior-driven-development-in-go">How to Use Godog for Behavior-driven Development in Go</a></td>
<td>Get started with Godog — a Behavior-driven development framework for building and testing Go applications.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/quii/learn-go-with-tests">Learn Go with TDD</a></td>
<td>Learn Go with test-driven development.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="https://www.youtube.com/packagemain">package main</a></td>
<td>YouTube channel about Programming in Go.</td>
</tr>

<tr>
<td>7654</td>
<td>32</td>
<td>1</td>
<td>2019-01-20</td>
<td><a href="https://github.com/mkaz/working-with-go">Working with Go</a></td>
<td>Intro to go for experienced programmers.</td>
</tr>

<tr>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td>N/A</td>
<td><a href="http://yourbasic.org/golang">Your basic Go</a></td>
<td>Huge collection of tutorials and how to&rsquo;s.</td>
</tr>
</tbody>
</table>
